﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace FixedDD.Models
{
    public class myAmdDB : DbContext
    {
        public virtual DbSet<FlightSearchResults> FlightSearchResults { get; set; }
        public virtual DbSet<AdtFareDetails> AdtFareDetails { get; set; }
        public virtual DbSet<CityList> CityList { get; set; }

        public virtual DbSet<AirLinesName> AirLinesName { get; set; }


    }
}