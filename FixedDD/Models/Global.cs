﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Text;
using System.Collections;

namespace FixedDD.Models
{
    public class Global
    {
        #region get Connecton
        public static string GetConnection
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
            }
        }
        #endregion

        #region get Source Data
        public static DataTable GetSourceData
        {
            get
            {
                SqlConnection con = new SqlConnection(GetConnection);
                SqlCommand cmd = new SqlCommand("SurceListProc", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
        }
        #endregion

        #region get Source Data Details
        public static DataTable GetSourceDataDetails(string val)
        {
            try
            {
                List<string> sc = val.Split(',').ToList();
                SqlConnection con = new SqlConnection(GetConnection);
                SqlCommand cmd = new SqlCommand("GetSuorceDetails", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@way", sc[0]);
                cmd.Parameters.AddWithValue("@Dest", sc[1]);
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch
            {
                return new DataTable();
            }
        }
        #endregion
        public static DataTable GetOrgDestFromSelect()
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
            SqlCommand cmd = new SqlCommand("GetOrgDestFrom", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }
        public static DataTable GetOrgDestFromTo()
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
            SqlCommand cmd = new SqlCommand("GetOrgDestTo", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }

        public static void ExportData(DataSet ds)
        {
            var FilterList = new string[] { "Red", "Blue" };
            HttpResponse Response = HttpContext.Current.Response;
            Response.Clear();
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.ContentType = "application/vnd.ms-excel";
            Response.Write("<!DOCTYPE HTML  PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">");
            string filename = "";
            if (!string.IsNullOrEmpty(filename))
            {
                Response.AddHeader("content-disposition", "attachment;filename=" + filename + ").xls");
            }
            else
            {
                Response.AddHeader("content-disposition", "attachment;filename=Report(" + "" + DateTime.Now.ToString("dd-MM-yyyyHHmmss") + ").xls");
            }

            Response.ContentEncoding = Encoding.UTF8;
            Response.Charset = "";
            // EnableViewState = False

            // Set Fonts
            Response.Write("<font style='font-size:10.0pt; font-family:Calibri;'>");
            Response.Write("<BR><BR><BR>");

            // Sets the table border, cell spacing, border color, font of the text, background,
            // foreground, font height
            Response.Write("<Table border='2' bgColor='#ffffff' borderColor='#000000' cellSpacing='0' cellPadding='0' style='font-size:10.0pt; font-family:Calibri; background:#F4F4F4;'> <TR>");

            // Check not to increase number of records more than 65k according to excel,03
            if (ds.Tables[0].Rows.Count <= 65536)
            {
                // Get DataTable Column's Header
                foreach (DataColumn column in ds.Tables[0].Columns)
                {
                    // Write in new column
                    Response.Write("<Td align='center'    style='padding-top: 7px;font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; background-color: #004b91; color: #FFFFFF;'>");

                    // Get column headers  and make it as bold in excel columns
                    Response.Write("<B >");
                    Response.Write(column);
                    Response.Write("</B>");
                    Response.Write("</Td>");
                }

                Response.Write("</TR>");

                // Get DataTable Column's Row
                foreach (DataRow dtRow in ds.Tables[0].Rows)
                {
                    // Write in new row
                    Response.Write("<TR>");
                    for (int i = 0, loopTo = ds.Tables[0].Columns.Count - 1; i <= loopTo; i++)
                    {
                        Response.Write("<Td>");
                        Response.Write(dtRow[i].ToString());
                        Response.Write("</Td>");
                    }

                    Response.Write("</TR>");
                }
            }

            Response.Write("</Table>");
            Response.Write("</font>");
            Response.Flush();
            Response.End();
        }

        public static List<string> InsertCreditDebitIntoLedgerTable(double Amount, string AgentId, string AgencyName, string InvoiceNo, string PnrNo, string TicketNo = "", string TicketingCarrier = "", string YatraAccountID = "", string AccountID = "", string ExecutiveID = "", string IPAddress = "", double Debit = 0, double Credit = 0, string BookingType = "", string Remark = "", int PaxId = 0, string Uploadtype = "", string YtrRcptNo = "", int ID = 0, string Status = "", string Type = "", string Rmk = "", string BankName = "", string BankCode = "", string Narration = "", int SplStatus = 0, string TransType = "")
        {
            List<string> HS = new List<string>();
            SqlDataAdapter adp;
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
            adp = new SqlDataAdapter("SP_INSERTUPLOADDETAILS_TRANSACTION", con);
            adp.SelectCommand.CommandType = CommandType.StoredProcedure;
            adp.SelectCommand.Parameters.AddWithValue("@Amount", Amount);
            adp.SelectCommand.Parameters.AddWithValue("@AgentId", AgentId);
            adp.SelectCommand.Parameters.AddWithValue("@AgencyName", AgencyName);
            adp.SelectCommand.Parameters.AddWithValue("@InvoiceNo", InvoiceNo);
            adp.SelectCommand.Parameters.AddWithValue("@PnrNo", PnrNo);
            adp.SelectCommand.Parameters.AddWithValue("@TicketNo", TicketNo);
            adp.SelectCommand.Parameters.AddWithValue("@TicketingCarrier", TicketingCarrier);
            adp.SelectCommand.Parameters.AddWithValue("@YatraAccountID", YatraAccountID);
            adp.SelectCommand.Parameters.AddWithValue("@AccountID", AccountID);
            adp.SelectCommand.Parameters.AddWithValue("@ExecutiveID", ExecutiveID);
            adp.SelectCommand.Parameters.AddWithValue("@IPAddress", IPAddress);
            adp.SelectCommand.Parameters.AddWithValue("@Debit", Debit);
            adp.SelectCommand.Parameters.AddWithValue("@Credit", Credit);
            adp.SelectCommand.Parameters.AddWithValue("@BookingType", BookingType);
            adp.SelectCommand.Parameters.AddWithValue("@Remark", Remark);
            adp.SelectCommand.Parameters.AddWithValue("@PaxId", PaxId);
            adp.SelectCommand.Parameters.AddWithValue("@Uploadtype", Uploadtype);
            adp.SelectCommand.Parameters.AddWithValue("@YtrRcptNo", YtrRcptNo);
            adp.SelectCommand.Parameters.AddWithValue("@ID", ID);
            adp.SelectCommand.Parameters.AddWithValue("@Status", Status);
            adp.SelectCommand.Parameters.AddWithValue("@Type", Type);
            adp.SelectCommand.Parameters.AddWithValue("@Rmk", Rmk);
            adp.SelectCommand.Parameters.AddWithValue("@SplStatus", SplStatus);
            adp.SelectCommand.Parameters.AddWithValue("@BankName", BankName);
            adp.SelectCommand.Parameters.AddWithValue("@BankCode", BankCode);
            adp.SelectCommand.Parameters.AddWithValue("@Narration", Narration);
            adp.SelectCommand.Parameters.AddWithValue("@TransType", TransType);
            adp.SelectCommand.Parameters.Add("@Aval_Balance", SqlDbType.Decimal);
            adp.SelectCommand.Parameters["@Aval_Balance"].Direction = ParameterDirection.Output;
            adp.SelectCommand.Parameters.Add("@result", SqlDbType.NVarChar, 500);
            adp.SelectCommand.Parameters["@result"].Direction = ParameterDirection.Output;
            adp.Fill(dt);
            string Result = adp.SelectCommand.Parameters["@result"].Value.ToString();
            decimal AvailableBalance = Convert.ToDecimal(adp.SelectCommand.Parameters["@Aval_Balance"].Value.ToString());
            HS.Add(Result);
            HS.Add(AvailableBalance.ToString());
            return HS;
        }

        public static string TicketCopyExportPDF(string OrderId, string TransID)
        {
            string TicketFormate = "";
            string strFileNmPdf = "";
            bool writePDF = false;
            string TktCopy = "";
            int Gtotal = 0;
            int initialAdt = 0;
            int initalChld = 0;
            int initialift = 0;
            decimal MealBagTotalPrice = 0;
            decimal AdtTtlFare = 0;
            decimal ChdTtlFare = 0;
            decimal INFTtlFare = 0;
            decimal fare = 0;
            DataTable FltPaxList = new DataTable();

            DataTable FltDetailsList = new DataTable();
            DataTable FltProvider = new DataTable();
            DataTable FltBaggage = new DataTable();
            DataTable dtagentid = new DataTable();
            DataTable FltagentDetail = new DataTable();
            DataTable fltTerminal = new DataTable();
            DataTable fltFare = new DataTable();
            DataTable fltMealAndBag = new DataTable();
            DataTable fltMealAndBag1 = new DataTable();
            DataSet fltAirportDetails = new DataSet();
            DataSet SelectedFltDS = new DataSet();
            DataTable ProxyCharge = new DataTable();
            DataTable FltHeaderList = new DataTable();


            FltPaxList = SelectPaxDetail(OrderId, TransID);
            FltHeaderList = SelectHeaderDetail(OrderId);
            FltDetailsList = SelectFlightDetail(OrderId);

            ProxyCharge = ProxyChargeMth(OrderId);
            dtagentid = SelectAgent(OrderId);
            SelectedFltDS = GetFltDtls(OrderId, dtagentid.Rows[0]["AgentID"].ToString());
            bool Bag = false;
            if (!string.IsNullOrEmpty(Convert.ToString(SelectedFltDS.Tables[0].Rows[0]["IsBagFare"])))
                Bag = Convert.ToBoolean(SelectedFltDS.Tables[0].Rows[0]["IsBagFare"]);
            FltBaggage = (GetBaggageInformation(Convert.ToString(FltHeaderList.Rows[0]["Trip"]), Convert.ToString(FltHeaderList.Rows[0]["VC"]), Bag)).Tables[0];

            FltagentDetail = SelectAgencyDetail(dtagentid.Rows[0]["AgentID"].ToString());

            fltFare = SelectFareDetail(OrderId, TransID);
            DateTime dt = Convert.ToDateTime(Convert.ToString(FltHeaderList.Rows[0]["CreateDate"]));
            string date = dt.ToString("dd/MMM/yyyy").Replace('-', '/');

            string Createddate = date.Split('/')[0] + " " + date.Split('/')[1] + " " + date.Split('/')[2];
            try
            {
                if (((Convert.ToString(FltHeaderList.Rows[0]["Status"]).ToLower().Trim() == "confirm" || Convert.ToString(FltHeaderList.Rows[0]["Status"]).ToLower().Trim() == "confirmbyagent")))
                {
                    TicketFormate += "<table style='width:100%;'>";
                    TicketFormate += "<tr>";
                    TicketFormate += "<td style='font-size: 15px; width: 15%; text-align: left; padding: 5px;'>";
                    TicketFormate += "<b>Booking Reference No. " + OrderId + "</b>";
                    TicketFormate += "</td>";
                    TicketFormate += "</tr>";
                    TicketFormate += "<tr>";
                    TicketFormate += "<td style='font-size: 14px; width: 15%; text-align: left; padding: 5px;'>";
                    TicketFormate += "The PNR-<b>" + FltHeaderList.Rows[0]["GdsPnr"] + " </b>is on <b>HOLD</b> and contact customer care for issuance.";
                    TicketFormate += "</td>";
                    TicketFormate += "</tr>";

                    TicketFormate += "<table style='border: 1px solid #0b2759; font-family: Verdana, Geneva, sans-serif; font-size: 12px;padding:0px !important;width:100%;'>";
                    TicketFormate += "<tr>";
                    TicketFormate += "<td style='text-align: left; background-color: #0b2759; color: #f58220; font-size: 11px; font-weight: bold; padding: 5px;' colspan='4'>";
                    TicketFormate += "Passenger Information";
                    TicketFormate += "</td>";
                    TicketFormate += "</tr>";
                    TicketFormate += "<tr>";
                    TicketFormate += "<td colspan='4' style='font-size:12px; padding: 5px; width: 100%'>";
                    TicketFormate += "<table>";

                    TicketFormate += "<tr>";
                    TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; padding: 5px;'>Issued By</td>";
                    TicketFormate += "<td style='font-size: 11px; width: 30%; text-align: left; padding: 5px;'>";
                    TicketFormate += FltHeaderList.Rows[0]["AgencyName"];
                    TicketFormate += "</td>";
                    TicketFormate += "</tr>";
                    TicketFormate += "<tr>";
                    TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; padding: 5px;'>Agency Info</td>";
                    TicketFormate += "<td style='font-size: 11px; width: 30%; text-align: left; padding: 5px;'>";
                    TicketFormate += FltagentDetail.Rows[0]["Mobile"];
                    TicketFormate += "<br/>";
                    TicketFormate += FltagentDetail.Rows[0]["Email"];
                    TicketFormate += "</td>";
                    TicketFormate += "</tr>";

                    TicketFormate += "<tr>";
                    TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; padding: 5px;'>Date Of Issue</td>";
                    TicketFormate += "<td style='font-size: 11px; width: 35%; text-align: left; padding: 5px;'>";
                    TicketFormate += Createddate;
                    TicketFormate += "</td>";
                    TicketFormate += "</tr>";
                    TicketFormate += "<tr>";
                    TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; padding: 5px;'></td>";
                    TicketFormate += "<td style='font-size: 13px; width: 30%; text-align: left; padding: 5px; font-weight: bold;'>";
                    TicketFormate += "</td>";
                    TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; padding: 5px;'>Customer Info</td>";
                    TicketFormate += "<td style='font-size: 11px; width: 30%; text-align: left; padding: 5px;'>";
                    TicketFormate += FltHeaderList.Rows[0]["PgMobile"];
                    TicketFormate += "<br/>";
                    TicketFormate += FltHeaderList.Rows[0]["PgEmail"];
                    TicketFormate += "</td>";
                    TicketFormate += "</tr>";
                    for (int p = 0; p <= FltPaxList.Rows.Count - 1; p++)
                    {
                        TicketFormate += "<tr>";
                        TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; padding: 5px;'>Passenger Name</td>";
                        TicketFormate += "<td style='font-size: 11px; width: 35%; text-align: left; padding: 5px;'>";
                        TicketFormate += FltPaxList.Rows[p]["Name"] + " " + "[" + FltPaxList.Rows[p]["PaxType"] + "]";
                        TicketFormate += "</td>";
                        TicketFormate += "<td style='font-size: 11px; width: 35%; text-align: left; padding: 5px;'>";
                        TicketFormate += FltPaxList.Rows[p]["TicketNumber"];
                        TicketFormate += "</td>";
                        TicketFormate += "</tr>";
                    }

                    TicketFormate += "</table>";
                    TicketFormate += "</td>";
                    TicketFormate += "</tr>";
                    TicketFormate += "<tr>";
                    TicketFormate += "<td style='text-align: left; background-color: #0b2759; color: #fff; width: 100%; padding: 5px;' colspan='4'>";
                    TicketFormate += "<table style='width:100%;'>";
                    TicketFormate += "<tr>";
                    TicketFormate += "<td style='text-align: left; color: #f58220; font-size: 11px; width: 25%;font-weight:bold;' colspan='1'>";
                    TicketFormate += "Flight Information";
                    TicketFormate += "</td>";
                    TicketFormate += "<td colspan='3' style='font-size: 11px; color: black; font-weight: bold; width: 75%; text-align: left; '></td>";
                    TicketFormate += "</tr>";
                    TicketFormate += "</table>";
                    TicketFormate += "</td>";
                    TicketFormate += "</tr>";

                    TicketFormate += "<tr>";
                    TicketFormate += "<td colspan='4' style='height:5px;'>&nbsp;</td>";
                    TicketFormate += "</tr>";

                    TicketFormate += "<tr>";
                    TicketFormate += "<td colspan='5' style='background-color: #0b2759;width:100%;'>";
                    TicketFormate += "<table style='width:100%;'>";
                    TicketFormate += "<tr>";
                    TicketFormate += "<td style='font-size: 10.5px; color: #fff; width: 10%; text-align: left; padding: 4px; font-weight: bold;'>FLIGHT</td>";
                    TicketFormate += "<td style='font-size: 10.5px; color: #fff; width: 20%; text-align: left; padding: 4px; font-weight: bold;'>DEPART</td>";
                    TicketFormate += "<td style='font-size: 10.5px; color: #fff; width: 20%; text-align: left; padding: 4px; font-weight: bold;'>ARRIVE</td>";
                    TicketFormate += "<td style='font-size: 10.5px; color: #fff; width: 25%; text-align: left; padding: 4px; font-weight: bold;'>DEPART AIRPORT/TERMINAL</td>";
                    TicketFormate += "<td style='font-size: 10.5px; color: #fff; width: 25%; text-align: left; padding: 4px; font-weight: bold;'>ARRIVE AIRPORT/TERMINAL</td>";
                    TicketFormate += "</tr>";

                    for (int f = 0; f <= FltDetailsList.Rows.Count - 1; f++)
                    {
                        TicketFormate += "</table>";
                        TicketFormate += "</td>";
                        TicketFormate += "</tr>";

                        TicketFormate += "<tr>";
                        TicketFormate += "<td colspan='5' style='width:100%;'>";
                        TicketFormate += "<table style='width:100%;'>";
                        TicketFormate += "<tr>";

                        TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; font-weight: bold; vertical-align: top;'>";
                        TicketFormate += FltDetailsList.Rows[f]["AirlineCode"] + " " + FltDetailsList.Rows[f]["FltNumber"];
                        TicketFormate += "<br/>";
                        TicketFormate += "<br/>";
                        TicketFormate += "<img alt='Logo Not Found' src='http://tripforo.com/AirLogo/sm" + FltDetailsList.Rows[f]["AirlineCode"] + ".gif' ></img>";
                        TicketFormate += "</td>";
                        TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; vertical-align: top;'>";
                        string strDepdt = Convert.ToString(FltDetailsList.Rows[f]["DepDate"]);
                        strDepdt = (strDepdt.Length == 8 ? Left(strDepdt, 4) + "-" + Mid(strDepdt, 4, 2) + "-" + Right(strDepdt, 2) : "20" + Right(strDepdt, 2) + "-" + Mid(strDepdt, 2, 2) + "-" + Left(strDepdt, 2));
                        DateTime deptdt = Convert.ToDateTime(strDepdt);
                        strDepdt = deptdt.ToString("dd/MMM/yy").Replace('-', '/');

                        // Response.Write(strDepdt)

                        string depDay = Convert.ToString(deptdt.DayOfWeek);
                        strDepdt = strDepdt.Split('/')[0] + " " + strDepdt.Split('/')[1] + " " + strDepdt.Split('/')[2];
                        string strdeptime = Convert.ToString(FltDetailsList.Rows[f]["DepTime"]);
                        // strdeptime = strdeptime.Substring(0, 2) + " : " + strdeptime.Substring(2, 2)
                        try
                        {
                            if (strdeptime.Length > 4)
                                strdeptime = strdeptime.Substring(0, 2) + " : " + strdeptime.Substring(3, 2);
                            else
                                strdeptime = strdeptime.Substring(0, 2) + " : " + strdeptime.Substring(2, 2);
                        }
                        catch (Exception ex)
                        {
                            ex.ToString();
                        }



                        TicketFormate += strDepdt;
                        TicketFormate += "<br/>";
                        TicketFormate += "<br/>";
                        TicketFormate += strdeptime;
                        TicketFormate += "</td>";

                        TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; vertical-align: top;'>";
                        string strArvdt = Convert.ToString(FltDetailsList.Rows[f]["ArrDate"]);
                        //strArvdt = IIf(strArvdt.Length == 8, Left(strArvdt, 4) + "-" + Mid(strArvdt, 4, 2) + "-" + Right(strArvdt, 2), "20" + Right(strArvdt, 2) + "-" + Mid(strArvdt, 2, 2) + "-" + Left(strArvdt, 2));
                        strArvdt = (strArvdt.Length == 8 ? Left(strArvdt, 4) + "-" + Mid(strArvdt, 4, 2) + "-" + Right(strArvdt, 2) : "20" + Right(strArvdt, 2) + "-" + Mid(strArvdt, 2, 2) + "-" + Left(strArvdt, 2));
                        DateTime Arrdt = Convert.ToDateTime(strArvdt);
                        strArvdt = Arrdt.ToString("dd/MMM/yy").Replace('-', '/');
                        string ArrDay = Convert.ToString(Arrdt.DayOfWeek);
                        strArvdt = strArvdt.Split('/')[0] + " " + strArvdt.Split('/')[1] + " " + strArvdt.Split('/')[2];
                        string strArrtime = Convert.ToString(FltDetailsList.Rows[f]["ArrTime"]);
                        // strArrtime = strArrtime.Substring(0, 2) + " : " + strArrtime.Substring(2, 2)
                        try
                        {
                            if (strArrtime.Length > 4)
                                strArrtime = strArrtime.Substring(0, 2) + " : " + strArrtime.Substring(3, 2);
                            else
                                strArrtime = strArrtime.Substring(0, 2) + " : " + strArrtime.Substring(2, 2);
                        }
                        catch (Exception ex)
                        {
                            ex.ToString();
                        }
                        TicketFormate += strArvdt;
                        TicketFormate += "<br/>";
                        TicketFormate += "<br/>";
                        TicketFormate += strArrtime;
                        TicketFormate += "</td>";
                        TicketFormate += "<td style='font-size: 11px; width: 25%; text-align: left; padding: 2px; font-weight: bold;'>";
                        TicketFormate += FltDetailsList.Rows[f]["DepAirName"] + "( " + FltDetailsList.Rows[f]["DFrom"] + "]";

                        TicketFormate += "<br />";
                        TicketFormate += "<br />";
                        //fltTerminalDetails = TerminalDetails(OrderId, FltDetailsList.Rows[f]["DFrom"].ToString(), "");
                        //if (string.IsNullOrEmpty(Convert.ToString(fltTerminalDetails.Rows[0]["DepartureTerminal"])))
                        //    TicketFormate += fltTerminalDetails.Rows[0]["DepAirportName"] + " - Trml: NA";
                        //else
                        //    TicketFormate += fltTerminalDetails.Rows[0]["DepAirportName"] + " - Trml:" + fltTerminalDetails.Rows[0]["DepartureTerminal"];
                        TicketFormate += "</td>";
                        TicketFormate += "<td style='font-size: 11px; width: 25%; text-align: left; padding: 2px; font-weight: bold;'>";
                        TicketFormate += FltDetailsList.Rows[f]["ArrAirName"] + " [" + FltDetailsList.Rows[f]["ATo"] + "]";
                        TicketFormate += "<br />";
                        TicketFormate += "<br />";
                        //fltTerminalDetails = TerminalDetails(OrderId, "", FltDetailsList.Rows[f]["ATo"].ToString());
                        //if (string.IsNullOrEmpty(Convert.ToString(fltTerminalDetails.Rows[0]["ArrivalTerminal"])))
                        //    TicketFormate += fltTerminalDetails.Rows[0]["ArrvlAirportName"] + " - Trml: NA";
                        //else
                        //    TicketFormate += fltTerminalDetails.Rows[0]["ArrvlAirportName"] + " - Trml:" + fltTerminalDetails.Rows[0]["ArrivalTerminal"];

                        TicketFormate += "</td>";
                        TicketFormate += "</tr>";
                        TicketFormate += "</table>";
                        TicketFormate += "</td>";
                        TicketFormate += "</tr>";

                        TicketFormate += "<tr>";
                        TicketFormate += "<td colspan='4' style='width:100%;'>";
                        TicketFormate += "<table style='width:100%;'>";
                        TicketFormate += "<tr>";
                        TicketFormate += "<td style='font-size: 11px; width: 322%; text-align: left; font-weight:bold;'>";
                        TicketFormate += "<br/>";
                        TicketFormate += "</td>";
                        TicketFormate += "<td style='width: 32%;'></td>";
                        TicketFormate += "<td style='width: 18%; font-size:11px;text-align:left;'></td>";
                        TicketFormate += "<td style='width: 18%; font-size: 11px; text-align: left; font-weight: bold;'></td>";
                        TicketFormate += "</tr>";
                    }
                    TicketFormate += "</table>";
                    TicketFormate += "</td>";
                    TicketFormate += "</table>";
                }
                else if ((Convert.ToString(FltHeaderList.Rows[0]["Status"]).ToLower().Trim() == "rejected"))
                {
                    TicketFormate += "<table style='width:100%;'>";
                    TicketFormate += "<tr>";
                    TicketFormate += "<td style='text-align:left;font-size:15px;'>";
                    TicketFormate += "<b>Booking Reference No. " + OrderId + "</b>";
                    TicketFormate += "</td>";
                    TicketFormate += "</tr>";
                    TicketFormate += "<tr>";
                    TicketFormate += "<td style='text-align:left;font-size:14px;'>";
                    TicketFormate += "Please re-try the booking.Your booking has been rejected due to some technical issue at airline end.";
                    TicketFormate += "</td>";
                    TicketFormate += "</tr>";

                    TicketFormate += "<table style='border: 1px solid #0b2759; font-family: Verdana, Geneva, sans-serif; font-size: 12px;padding:0px !important;width:100%;'>";
                    TicketFormate += "<tr>";
                    TicketFormate += "<td style='text-align: left; background-color: #0b2759; color: #f58220; font-size: 11px; font-weight: bold; padding: 5px;' colspan='4'>";
                    TicketFormate += "Passenger & Ticket Information";
                    TicketFormate += "</td>";
                    TicketFormate += "</tr>";
                    TicketFormate += "<tr>";
                    TicketFormate += "<td colspan='4' style='font-size:12px; padding: 5px; width: 100%'>";
                    TicketFormate += "<table>";

                    TicketFormate += "<tr>";
                    TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; padding: 5px;'>GDS PNR</td>";
                    TicketFormate += "<td style='font-size: 11px; width: 35%; text-align: left; padding: 5px;'>";
                    TicketFormate += FltHeaderList.Rows[0]["GdsPnr"];
                    TicketFormate += "</td>";
                    TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; padding: 5px;'>Issued By</td>";
                    TicketFormate += "<td style='font-size: 11px; width: 30%; text-align: left; padding: 5px;'>";
                    TicketFormate += FltHeaderList.Rows[0]["AgencyName"];
                    TicketFormate += "</td>";
                    TicketFormate += "</tr>";

                    TicketFormate += "<tr>";
                    TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; padding: 5px;'>Airline PNR</td>";
                    TicketFormate += "<td style='font-size: 11px; width: 35%; text-align: left; padding: 5px;'>";
                    TicketFormate += FltHeaderList.Rows[0]["AirlinePnr"];
                    TicketFormate += "</td>";
                    TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; padding: 5px;'>Agency Info</td>";
                    TicketFormate += "<td style='font-size: 11px; width: 30%; text-align: left; padding: 5px;'>";
                    TicketFormate += FltagentDetail.Rows[0]["Mobile"];
                    TicketFormate += "<br/>";
                    TicketFormate += FltagentDetail.Rows[0]["Email"];
                    TicketFormate += "</td>";
                    TicketFormate += "</tr>";

                    TicketFormate += "<tr>";
                    TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; padding: 5px;'>Status</td>";
                    TicketFormate += "<td style='font-size: 11px; width: 30%; text-align: left; padding: 5px;'>";
                    //TicketFormate += IIf(Convert.ToString(FltHeaderList.Rows[0]["Status"]).ToLower().Trim() == "confirm", "Hold", FltHeaderList.Rows[0]["Status"]);


                    TicketFormate += (Convert.ToString(FltHeaderList.Rows[0]["Status"]).ToLower().Trim() == "confirm" ? "Hold" : FltHeaderList.Rows[0]["Status"]);
                    TicketFormate += "</td>";
                    TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; padding: 5px;'>Date Of Issue</td>";
                    TicketFormate += "<td style='font-size: 11px; width: 35%; text-align: left; padding: 5px;'>";
                    TicketFormate += Createddate;
                    TicketFormate += "</td>";
                    TicketFormate += "</tr>";


                    TicketFormate += "<tr>";
                    TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; padding: 5px;'></td>"; // FareType
                    TicketFormate += "<td style='font-size: 13px; width: 30%; text-align: left; padding: 5px; font-weight: bold;'>";
                    // TicketFormate += Convert.ToString(SelectedFltDS.Tables[0].Rows[0]["AdtFareType"])
                    TicketFormate += "</td>";
                    TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; padding: 5px;'>Customer Info</td>";
                    TicketFormate += "<td style='font-size: 11px; width: 30%; text-align: left; padding: 5px;'>";
                    TicketFormate += FltHeaderList.Rows[0]["PgMobile"];
                    TicketFormate += "<br/>";
                    TicketFormate += FltHeaderList.Rows[0]["PgEmail"];
                    TicketFormate += "</td>";
                    TicketFormate += "</tr>";

                    for (int p = 0; p <= FltPaxList.Rows.Count - 1; p++)
                    {
                        TicketFormate += "<tr>";
                        TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; padding: 5px;'>Passenger Name</td>";
                        TicketFormate += "<td style='font-size: 11px; width: 35%; text-align: left; padding: 5px;'>";
                        TicketFormate += FltPaxList.Rows[p]["Name"] + " " + "[" + FltPaxList.Rows[p]["PaxType"] + "]";
                        TicketFormate += "</td>";
                        TicketFormate += "<td style='font-size: 11px; width: 35%; text-align: left; padding: 5px;'>";
                        TicketFormate += FltPaxList.Rows[p]["TicketNumber"];
                        TicketFormate += "</td>";
                        TicketFormate += "</tr>";
                    }

                    TicketFormate += "</table>";
                    TicketFormate += "</td>";
                    TicketFormate += "</tr>";
                    TicketFormate += "<tr>";
                    TicketFormate += "<td style='text-align: left; background-color: #0b2759; color: #fff; width: 100%; padding: 5px;' colspan='4'>";
                    TicketFormate += "<table style='width:100%;'>";
                    TicketFormate += "<tr>";
                    TicketFormate += "<td style='text-align: left; color: #f58220; font-size: 11px; width: 25%;font-weight:bold;' colspan='1'>";
                    TicketFormate += "Flight Information";
                    TicketFormate += "</td>";
                    TicketFormate += "<td colspan='3' style='font-size: 11px; color: black; font-weight: bold; width: 75%; text-align: left; '></td>";
                    TicketFormate += "</tr>";
                    TicketFormate += "</table>";
                    TicketFormate += "</td>";
                    TicketFormate += "</tr>";

                    TicketFormate += "<tr>";
                    TicketFormate += "<td colspan='4' style='height:5px;'>&nbsp;</td>";
                    TicketFormate += "</tr>";

                    TicketFormate += "<tr>";
                    TicketFormate += "<td colspan='5' style='background-color: #0b2759;width:100%;'>";
                    TicketFormate += "<table style='width:100%;'>";
                    TicketFormate += "<tr>";
                    TicketFormate += "<td style='font-size: 10.5px; color: #fff; width: 10%; text-align: left; padding: 4px; font-weight: bold;'>FLIGHT</td>";
                    TicketFormate += "<td style='font-size: 10.5px; color: #fff; width: 20%; text-align: left; padding: 4px; font-weight: bold;'>DEPART</td>";
                    TicketFormate += "<td style='font-size: 10.5px; color: #fff; width: 20%; text-align: left; padding: 4px; font-weight: bold;'>ARRIVE</td>";
                    TicketFormate += "<td style='font-size: 10.5px; color: #fff; width: 25%; text-align: left; padding: 4px; font-weight: bold;'>DEPART AIRPORT/TERMINAL</td>";
                    TicketFormate += "<td style='font-size: 10.5px; color: #fff; width: 25%; text-align: left; padding: 4px; font-weight: bold;'>ARRIVE AIRPORT/TERMINAL</td>";
                    TicketFormate += "</tr>";

                    for (int f = 0; f <= FltDetailsList.Rows.Count - 1; f++)
                    {
                        TicketFormate += "</table>";
                        TicketFormate += "</td>";
                        TicketFormate += "</tr>";
                        TicketFormate += "<tr>";
                        TicketFormate += "<td colspan='5' style='width:100%;'>";
                        TicketFormate += "<table style='width:100%;'>";
                        TicketFormate += "<tr>";
                        TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; font-weight: bold; vertical-align: top;'>";
                        TicketFormate += FltDetailsList.Rows[f]["AirlineCode"] + " " + FltDetailsList.Rows[f]["FltNumber"];
                        TicketFormate += "<br/>";
                        TicketFormate += "<br/>";
                        TicketFormate += "<img alt='Logo Not Found' src='http://Tripforo.com/AirLogo/sm" + FltDetailsList.Rows[f]["AirlineCode"] + ".gif' ></img>";
                        TicketFormate += "</td>";
                        TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; vertical-align: top;'>";
                        string strDepdt = Convert.ToString(FltDetailsList.Rows[f]["DepDate"]);
                        //strDepdt = IIf(strDepdt.Length == 8, Left(strDepdt, 4) + "-" + Mid(strDepdt, 4, 2) + "-" + Right(strDepdt, 2), "20" + Right(strDepdt, 2) + "-" + Mid(strDepdt, 2, 2) + "-" + Left(strDepdt, 2));

                        strDepdt = (strDepdt.Length == 8 ? Left(strDepdt, 4) + "-" + Mid(strDepdt, 4, 2) + "-" + Right(strDepdt, 2) : "20" + Right(strDepdt, 2) + "-" + Mid(strDepdt, 2, 2) + "-" + Left(strDepdt, 2));
                        DateTime deptdt = Convert.ToDateTime(strDepdt);
                        strDepdt = deptdt.ToString("dd/MMM/yy").Replace('-', '/');

                        // 'Response.Write(strDepdt)

                        string depDay = Convert.ToString(deptdt.DayOfWeek);
                        strDepdt = strDepdt.Split('/')[0] + " " + strDepdt.Split('/')[1] + " " + strDepdt.Split('/')[2];
                        string strdeptime = Convert.ToString(FltDetailsList.Rows[f]["DepTime"]);
                        // strdeptime = strdeptime.Substring(0, 2) + " : " + strdeptime.Substring(2, 2)
                        try
                        {
                            if (strdeptime.Length > 4)
                                strdeptime = strdeptime.Substring(0, 2) + " : " + strdeptime.Substring(3, 2);
                            else
                                strdeptime = strdeptime.Substring(0, 2) + " : " + strdeptime.Substring(2, 2);
                        }
                        catch (Exception ex)
                        {
                            ex.ToString();
                        }
                        TicketFormate += strDepdt;
                        TicketFormate += "<br/>";
                        TicketFormate += "<br/>";
                        TicketFormate += strdeptime;
                        TicketFormate += "</td>";
                        TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; vertical-align: top;'>";
                        string strArvdt = Convert.ToString(FltDetailsList.Rows[f]["ArrDate"]);
                        //strArvdt = IIf(strArvdt.Length == 8, Left(strArvdt, 4) + "-" + Mid(strArvdt, 4, 2) + "-" + Right(strArvdt, 2), "20" + Right(strArvdt, 2) + "-" + Mid(strArvdt, 2, 2) + "-" + Left(strArvdt, 2));
                        strArvdt = (strArvdt.Length == 8 ? Left(strArvdt, 4) + "-" + Mid(strArvdt, 4, 2) + "-" + Right(strArvdt, 2) : "20" + Right(strArvdt, 2) + "-" + Mid(strArvdt, 2, 2) + "-" + Left(strArvdt, 2));
                        DateTime Arrdt = Convert.ToDateTime(strArvdt);
                        strArvdt = Arrdt.ToString("dd/MMM/yy").Replace('-', '/');
                        string ArrDay = Convert.ToString(Arrdt.DayOfWeek);
                        strArvdt = strArvdt.Split('/')[0] + " " + strArvdt.Split('/')[1] + " " + strArvdt.Split('/')[2];
                        string strArrtime = Convert.ToString(FltDetailsList.Rows[f]["ArrTime"]);
                        // strArrtime = strArrtime.Substring(0, 2) + " : " + strArrtime.Substring(2, 2)
                        try
                        {
                            if (strArrtime.Length > 4)
                                strArrtime = strArrtime.Substring(0, 2) + " : " + strArrtime.Substring(3, 2);
                            else
                                strArrtime = strArrtime.Substring(0, 2) + " : " + strArrtime.Substring(2, 2);
                        }
                        catch (Exception ex)
                        {
                            ex.ToString();
                        }
                        TicketFormate += strArvdt;
                        TicketFormate += "<br/>";
                        TicketFormate += "<br/>";
                        TicketFormate += strArrtime;
                        TicketFormate += "</td>";
                        TicketFormate += "<td style='font-size: 11px; width: 25%; text-align: left; padding: 2px; font-weight: bold;'>";
                        TicketFormate += FltDetailsList.Rows[f]["DepAirName"] + "( " + FltDetailsList.Rows[f]["DFrom"] + "]";
                        TicketFormate += "<br />";
                        TicketFormate += "<br />";
                        //fltTerminalDetails = TerminalDetails(OrderId, FltDetailsList.Rows[f]["DFrom"].ToString(), "");
                        //if (string.IsNullOrEmpty(Convert.ToString(fltTerminalDetails.Rows[0]["DepartureTerminal"])))
                        //    TicketFormate += fltTerminalDetails.Rows[0]["DepAirportName"] + " - Trml: NA";
                        //else
                        //    TicketFormate += fltTerminalDetails.Rows[0]["DepAirportName"] + " - Trml:" + fltTerminalDetails.Rows[0]["DepartureTerminal"];
                        TicketFormate += "</td>";
                        TicketFormate += "<td style='font-size: 11px; width: 25%; text-align: left; padding: 2px; font-weight: bold;'>";
                        TicketFormate += FltDetailsList.Rows[f]["ArrAirName"] + " [" + FltDetailsList.Rows[f]["ATo"] + "]";
                        TicketFormate += "<br />";
                        TicketFormate += "<br />";
                        //fltTerminalDetails = TerminalDetails(OrderId, "", FltDetailsList.Rows[f]["ATo"].ToString());
                        //if (string.IsNullOrEmpty(Convert.ToString(fltTerminalDetails.Rows[0]["ArrivalTerminal"])))
                        //    TicketFormate += fltTerminalDetails.Rows[0]["ArrvlAirportName"] + " - Trml: NA";
                        //else
                        //    TicketFormate += fltTerminalDetails.Rows[0]["ArrvlAirportName"] + " - Trml:" + fltTerminalDetails.Rows[0]["ArrivalTerminal"];
                        TicketFormate += "</td>";
                        TicketFormate += "</tr>";
                        TicketFormate += "</table>";
                        TicketFormate += "</td>";
                        TicketFormate += "</tr>";
                        TicketFormate += "<tr>";
                        TicketFormate += "<td colspan='4' style='width:100%;'>";
                        TicketFormate += "<table style='width:100%;'>";
                        TicketFormate += "<tr>";
                        TicketFormate += "<td style='font-size: 11px; width: 322%; text-align: left; font-weight:bold;'>";
                        TicketFormate += "<br/>";
                        TicketFormate += "</td>";
                        TicketFormate += "<td style='width: 32%;'></td>";
                        TicketFormate += "<td style='width: 18%; font-size:11px;text-align:left;'></td>";
                        TicketFormate += "<td style='width: 18%; font-size: 11px; text-align: left; font-weight: bold;'></td>";
                        TicketFormate += "</tr>";
                    }
                    TicketFormate += "</table>";
                    TicketFormate += "</td>";
                    TicketFormate += "</table>";
                }
                else if ((Convert.ToString(FltHeaderList.Rows[0]["Status"]).ToLower().Trim() == "inprocess"))
                {
                    TicketFormate += "<table style='width:100%;'>";
                    TicketFormate += "<tr>";
                    TicketFormate += "<td style='text-align:center;font-size:15px;'>";
                    TicketFormate += "<b>Booking Reference No. " + OrderId + "</b>";
                    TicketFormate += "</td>";
                    TicketFormate += "</tr>";
                    TicketFormate += "<tr>";
                    TicketFormate += "<td style='text-align:left;font-size:14px;'>";
                    TicketFormate += "We are updating the details, Please wait for some time.";
                    TicketFormate += "</td>";
                    TicketFormate += "</tr>";

                    TicketFormate += "<table style='border: 1px solid #0b2759; font-family: Verdana, Geneva, sans-serif; font-size: 12px;padding:0px !important;width:100%;'>";
                    TicketFormate += "<tr>";
                    TicketFormate += "<td style='text-align: left; background-color: #0b2759; color: #f58220; font-size: 11px; font-weight: bold; padding: 5px;' colspan='4'>";
                    TicketFormate += "Passenger & Ticket Information";
                    TicketFormate += "</td>";
                    TicketFormate += "</tr>";
                    TicketFormate += "<tr>";
                    TicketFormate += "<td colspan='4' style='font-size:12px; padding: 5px; width: 100%'>";
                    TicketFormate += "<table>";

                    TicketFormate += "<tr>";
                    TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; padding: 5px;'>GDS PNR</td>";
                    TicketFormate += "<td style='font-size: 11px; width: 35%; text-align: left; padding: 5px;'>";
                    TicketFormate += FltHeaderList.Rows[0]["GdsPnr"];
                    TicketFormate += "</td>";
                    TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; padding: 5px;'>Issued By</td>";
                    TicketFormate += "<td style='font-size: 11px; width: 30%; text-align: left; padding: 5px;'>";
                    TicketFormate += FltHeaderList.Rows[0]["AgencyName"];
                    TicketFormate += "</td>";
                    TicketFormate += "</tr>";

                    TicketFormate += "<tr>";
                    TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; padding: 5px;'>Airline PNR</td>";
                    TicketFormate += "<td style='font-size: 11px; width: 35%; text-align: left; padding: 5px;'>";
                    TicketFormate += FltHeaderList.Rows[0]["AirlinePnr"];
                    TicketFormate += "</td>";
                    TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; padding: 5px;'>Agency Info</td>";
                    TicketFormate += "<td style='font-size: 11px; width: 30%; text-align: left; padding: 5px;'>";
                    TicketFormate += FltagentDetail.Rows[0]["Mobile"];
                    TicketFormate += "<br/>";
                    TicketFormate += FltagentDetail.Rows[0]["Email"];
                    TicketFormate += "</td>";
                    TicketFormate += "</tr>";

                    TicketFormate += "<tr>";
                    TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; padding: 5px;'>Status</td>";
                    TicketFormate += "<td style='font-size: 11px; width: 30%; text-align: left; padding: 5px;'>";
                    TicketFormate += (Convert.ToString(FltHeaderList.Rows[0]["Status"]).ToLower().Trim() == "confirm" ? "Hold" : FltHeaderList.Rows[0]["Status"]);
                    TicketFormate += "</td>";
                    TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; padding: 5px;'>Date Of Issue</td>";
                    TicketFormate += "<td style='font-size: 11px; width: 35%; text-align: left; padding: 5px;'>";
                    TicketFormate += Createddate;
                    TicketFormate += "</td>";

                    TicketFormate += "</tr>";
                    TicketFormate += "<tr>";
                    TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; padding: 5px;'></td>"; // FareType
                    TicketFormate += "<td style='font-size: 13px; width: 30%; text-align: left; padding: 5px; font-weight: bold;'>";
                    TicketFormate += "</td>";
                    TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; padding: 5px;'>Customer Info</td>";
                    TicketFormate += "<td style='font-size: 11px; width: 30%; text-align: left; padding: 5px;'>";
                    TicketFormate += FltHeaderList.Rows[0]["PgMobile"];
                    TicketFormate += "<br/>";
                    TicketFormate += FltHeaderList.Rows[0]["PgEmail"];
                    TicketFormate += "</td>";
                    TicketFormate += "</tr>";


                    for (int p = 0; p <= FltPaxList.Rows.Count - 1; p++)
                    {
                        TicketFormate += "<tr>";
                        TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; padding: 5px;'>Passenger Name</td>";
                        TicketFormate += "<td style='font-size: 11px; width: 35%; text-align: left; padding: 5px;'>";
                        TicketFormate += FltPaxList.Rows[p]["Name"] + " " + "[" + FltPaxList.Rows[p]["PaxType"] + "]";
                        TicketFormate += "</td>";
                        TicketFormate += "<td style='font-size: 11px; width: 35%; text-align: left; padding: 5px;'>";
                        TicketFormate += FltPaxList.Rows[p]["TicketNumber"];
                        TicketFormate += "</td>";
                        TicketFormate += "</tr>";
                    }
                    TicketFormate += "</table>";
                    TicketFormate += "</td>";
                    TicketFormate += "</tr>";
                    TicketFormate += "<tr>";
                    TicketFormate += "<td style='text-align: left; background-color: #0b2759; color: #fff; width: 100%; padding: 5px;' colspan='4'>";
                    TicketFormate += "<table style='width:100%;'>";
                    TicketFormate += "<tr>";
                    TicketFormate += "<td style='text-align: left; color: #f58220; font-size: 11px; width: 25%;font-weight:bold;' colspan='1'>";
                    TicketFormate += "Flight Information";
                    TicketFormate += "</td>";
                    TicketFormate += "<td colspan='3' style='font-size: 11px; color: black; font-weight: bold; width: 75%; text-align: left; '></td>";
                    TicketFormate += "</tr>";
                    TicketFormate += "</table>";
                    TicketFormate += "</td>";
                    TicketFormate += "</tr>";
                    TicketFormate += "<tr>";
                    TicketFormate += "<td colspan='4' style='height:5px;'>&nbsp;</td>";
                    TicketFormate += "</tr>";
                    TicketFormate += "<tr>";
                    TicketFormate += "<td colspan='5' style='background-color: #0b2759;width:100%;'>";
                    TicketFormate += "<table style='width:100%;'>";
                    TicketFormate += "<tr>";
                    TicketFormate += "<td style='font-size: 10.5px; color: #fff; width: 10%; text-align: left; padding: 4px; font-weight: bold;'>FLIGHT</td>";
                    TicketFormate += "<td style='font-size: 10.5px; color: #fff; width: 20%; text-align: left; padding: 4px; font-weight: bold;'>DEPART</td>";
                    TicketFormate += "<td style='font-size: 10.5px; color: #fff; width: 20%; text-align: left; padding: 4px; font-weight: bold;'>ARRIVE</td>";
                    TicketFormate += "<td style='font-size: 10.5px; color: #fff; width: 25%; text-align: left; padding: 4px; font-weight: bold;'>DEPART AIRPORT/TERMINAL</td>";
                    TicketFormate += "<td style='font-size: 10.5px; color: #fff; width: 25%; text-align: left; padding: 4px; font-weight: bold;'>ARRIVE AIRPORT/TERMINAL</td>";
                    TicketFormate += "</tr>";

                    for (int f = 0; f <= FltDetailsList.Rows.Count - 1; f++)
                    {
                        TicketFormate += "</table>";
                        TicketFormate += "</td>";
                        TicketFormate += "</tr>";
                        TicketFormate += "<tr>";
                        TicketFormate += "<td colspan='5' style='width:100%;'>";
                        TicketFormate += "<table style='width:100%;'>";
                        TicketFormate += "<tr>";
                        TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; font-weight: bold; vertical-align: top;'>";
                        TicketFormate += FltDetailsList.Rows[f]["AirlineCode"] + " " + FltDetailsList.Rows[f]["FltNumber"];
                        TicketFormate += "<br/>";
                        TicketFormate += "<br/>";
                        TicketFormate += "<img alt='Logo Not Found' src='http://Tripforo.com/AirLogo/sm" + FltDetailsList.Rows[f]["AirlineCode"] + ".gif' ></img>";
                        TicketFormate += "</td>";
                        TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; vertical-align: top;'>";
                        string strDepdt = Convert.ToString(FltDetailsList.Rows[f]["DepDate"]);
                        //strDepdt = IIf(strDepdt.Length == 8, Left(strDepdt, 4) + "-" + Mid(strDepdt, 4, 2) + "-" + Right(strDepdt, 2), "20" + Right(strDepdt, 2) + "-" + Mid(strDepdt, 2, 2) + "-" + Left(strDepdt, 2));

                        strDepdt = (strDepdt.Length == 8 ? Left(strDepdt, 4) + "-" + Mid(strDepdt, 4, 2) + "-" + Right(strDepdt, 2) : "20" + Right(strDepdt, 2) + "-" + Mid(strDepdt, 2, 2) + "-" + Left(strDepdt, 2));
                        DateTime deptdt = Convert.ToDateTime(strDepdt);
                        strDepdt = deptdt.ToString("dd/MMM/yy").Replace('-', '/');

                        string depDay = Convert.ToString(deptdt.DayOfWeek);
                        strDepdt = strDepdt.Split('/')[0] + " " + strDepdt.Split('/')[1] + " " + strDepdt.Split('/')[2];
                        string strdeptime = Convert.ToString(FltDetailsList.Rows[f]["DepTime"]);
                        // strdeptime = strdeptime.Substring(0, 2) + " : " + strdeptime.Substring(2, 2)

                        try
                        {
                            if (strdeptime.Length > 4)
                                strdeptime = strdeptime.Substring(0, 2) + " : " + strdeptime.Substring(3, 2);
                            else
                                strdeptime = strdeptime.Substring(0, 2) + " : " + strdeptime.Substring(2, 2);
                        }
                        catch (Exception ex)
                        {
                            ex.ToString();
                        }
                        TicketFormate += strDepdt;
                        TicketFormate += "<br/>";
                        TicketFormate += "<br/>";
                        TicketFormate += strdeptime;
                        TicketFormate += "</td>";

                        TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; vertical-align: top;'>";
                        string strArvdt = Convert.ToString(FltDetailsList.Rows[f]["ArrDate"]);
                        //strArvdt = IIf(strArvdt.Length == 8, Left(strArvdt, 4) + "-" + Mid(strArvdt, 4, 2) + "-" + Right(strArvdt, 2), "20" + Right(strArvdt, 2) + "-" + Mid(strArvdt, 2, 2) + "-" + Left(strArvdt, 2));

                        strArvdt = (strArvdt.Length == 8 ? Left(strArvdt, 4) + "-" + Mid(strArvdt, 4, 2) + "-" + Right(strArvdt, 2) : "20" + Right(strArvdt, 2) + "-" + Mid(strArvdt, 2, 2) + "-" + Left(strArvdt, 2));
                        DateTime Arrdt = Convert.ToDateTime(strArvdt);
                        strArvdt = Arrdt.ToString("dd/MMM/yy").Replace('-', '/');
                        string ArrDay = Convert.ToString(Arrdt.DayOfWeek);
                        strArvdt = strArvdt.Split('/')[0] + " " + strArvdt.Split('/')[1] + " " + strArvdt.Split('/')[2];
                        string strArrtime = Convert.ToString(FltDetailsList.Rows[f]["ArrTime"]);
                        // strArrtime = strArrtime.Substring(0, 2) + " : " + strArrtime.Substring(2, 2)
                        try
                        {
                            if (strArrtime.Length > 4)
                                strArrtime = strArrtime.Substring(0, 2) + " : " + strArrtime.Substring(3, 2);
                            else
                                strArrtime = strArrtime.Substring(0, 2) + " : " + strArrtime.Substring(2, 2);
                        }
                        catch (Exception ex)
                        {
                            ex.ToString();
                        }
                        TicketFormate += strArvdt;
                        TicketFormate += "<br/>";
                        TicketFormate += "<br/>";
                        TicketFormate += strArrtime;
                        TicketFormate += "</td>";
                        TicketFormate += "<td style='font-size: 11px; width: 25%; text-align: left; padding: 2px; font-weight: bold;'>";
                        TicketFormate += FltDetailsList.Rows[f]["DepAirName"] + "( " + FltDetailsList.Rows[f]["DFrom"] + "]";

                        TicketFormate += "<br />";
                        TicketFormate += "<br />";
                        //fltTerminalDetails = TerminalDetails(OrderId, FltDetailsList.Rows[f]["DFrom"].ToString(), "");
                        //if (string.IsNullOrEmpty(Convert.ToString(fltTerminalDetails.Rows[0]["DepartureTerminal"])))
                        //    TicketFormate += fltTerminalDetails.Rows[0]["DepAirportName"] + " - Trml: NA";
                        //else
                        //    TicketFormate += fltTerminalDetails.Rows[0]["DepAirportName"] + " - Trml:" + fltTerminalDetails.Rows[0]["DepartureTerminal"];

                        TicketFormate += "</td>";
                        TicketFormate += "<td style='font-size: 11px; width: 25%; text-align: left; padding: 2px; font-weight: bold;'>";
                        TicketFormate += FltDetailsList.Rows[f]["ArrAirName"] + " [" + FltDetailsList.Rows[f]["ATo"] + "]";
                        TicketFormate += "<br />";
                        TicketFormate += "<br />";
                        //fltTerminalDetails = TerminalDetails(OrderId, "", FltDetailsList.Rows[f]["ATo"].ToString());
                        //if (string.IsNullOrEmpty(Convert.ToString(fltTerminalDetails.Rows[0]["ArrivalTerminal"])))
                        //    TicketFormate += fltTerminalDetails.Rows[0]["ArrvlAirportName"] + " - Trml: NA";
                        //else
                        //    TicketFormate += fltTerminalDetails.Rows[0]["ArrvlAirportName"] + " - Trml:" + fltTerminalDetails.Rows[0]["ArrivalTerminal"];

                        TicketFormate += "</td>";
                        TicketFormate += "</tr>";
                        TicketFormate += "</table>";
                        TicketFormate += "</td>";
                        TicketFormate += "</tr>";

                        TicketFormate += "<tr>";
                        TicketFormate += "<td colspan='4' style='width:100%;'>";
                        TicketFormate += "<table style='width:100%;'>";
                        TicketFormate += "<tr>";
                        TicketFormate += "<td style='font-size: 11px; width: 322%; text-align: left; font-weight:bold;'>";
                        // TicketFormate += "<img alt='Logo Not Found' src='http://flywidus.co/AirLogo/sm" + FltDetailsList.Rows[f]["AirlineCode"] + ".gif' ></img>"
                        TicketFormate += "<br/>";
                        // TicketFormate += FltDetailsList.Rows[f]["AirlineName"]
                        TicketFormate += "</td>";
                        TicketFormate += "<td style='width: 32%;'></td>";
                        TicketFormate += "<td style='width: 18%; font-size:11px;text-align:left;'></td>";
                        TicketFormate += "<td style='width: 18%; font-size: 11px; text-align: left; font-weight: bold;'></td>";
                        TicketFormate += "</tr>";
                    }
                    TicketFormate += "</table>";
                    TicketFormate += "</td>";
                    TicketFormate += "</table>";
                }
                else
                {
                    TicketFormate += "<table style='width:100%;'>";
                    TicketFormate += "<tr>";
                    TicketFormate += "<td style='width:50%;text-align:left;'>";
                    TicketFormate += "<img src='http://tripforo.com/Advance_CSS/Icons/logo(ft).png' alt='Logo' style='height:70px;width:200px'/>";
                    TicketFormate += "</td>";
                    TicketFormate += "<td style='width: 50%;text-align:right;display:none;'>";
                    TicketFormate += "</td>";
                    TicketFormate += "</tr>";
                    TicketFormate += "<tr>";
                    TicketFormate += "<td style='width:50%;text-align:left;'>";
                    TicketFormate += "";
                    TicketFormate += "</td>";
                    TicketFormate += "<td style='width: 50%;text-align:right;'>";
                    TicketFormate += "";
                    TicketFormate += "</td>";
                    TicketFormate += "</tr>";
                    TicketFormate += "<tr>";
                    TicketFormate += "<td style='width:100%;height:10px;'></td>";
                    TicketFormate += "</tr>";
                    TicketFormate += "<tr>";
                    TicketFormate += "<td colspan='2' style='vertical-align:bottom;color:#f58220;text-align:right;width:100%;font-size:16px;font-weight:bold;'>";
                    TicketFormate += "Electronic Ticket";
                    TicketFormate += "</td>";
                    TicketFormate += "</tr>";
                    TicketFormate += "<tr>";
                    TicketFormate += "<td colspan='2' style='height: 2px; width: 100%; border: 1px solid #0b2759'></td>";
                    TicketFormate += "</tr>";
                    TicketFormate += "</table>";


                    TicketFormate += "<table style='width: 100%;'>";
                    TicketFormate += "<tr>";
                    TicketFormate += "<td style='width: 100%; text-align: justify; color: #0b2759; font-size: 11px; padding: 10px;'>";
                    TicketFormate += "This is travel itinerary and E-ticket receipt. You may need to show this receipt to enter the airport and/or to show return or onward travel to ";
                    TicketFormate += "customs and immigration officials.";
                    TicketFormate += "<br />";

                    TicketFormate += "</td>";
                    TicketFormate += "</tr>";
                    TicketFormate += "</table>";

                    TicketFormate += "<table style='border: 1px solid #0b2759; font-family: Verdana, Geneva, sans-serif; font-size: 12px;padding:0px !important;width:100%;'>";
                    TicketFormate += "<tr>";
                    TicketFormate += "<td style='text-align: left; background-color: #0b2759; color: #f58220; font-size: 11px; font-weight: bold; padding: 5px;' colspan='4'>";
                    TicketFormate += "Passenger & Ticket Information";
                    TicketFormate += "</td>";
                    TicketFormate += "</tr>";
                    TicketFormate += "<tr>";
                    TicketFormate += "<td colspan='4' style='font-size:12px; padding: 5px; width: 100%'>";
                    TicketFormate += "<table>";

                    TicketFormate += "<tr>";
                    TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; padding: 5px;'>GDS PNR</td>";
                    TicketFormate += "<td style='font-size: 11px; width: 35%; text-align: left; padding: 5px;'>";
                    TicketFormate += FltHeaderList.Rows[0]["GdsPnr"];
                    TicketFormate += "</td>";
                    TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; padding: 5px;'>Issued By</td>";
                    TicketFormate += "<td style='font-size: 11px; width: 30%; text-align: left; padding: 5px;'>";
                    TicketFormate += FltHeaderList.Rows[0]["AgencyName"];
                    TicketFormate += "</td>";
                    TicketFormate += "</tr>";

                    TicketFormate += "<tr>";
                    TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; padding: 5px;'>Airline PNR</td>";
                    TicketFormate += "<td style='font-size: 11px; width: 35%; text-align: left; padding: 5px;'>";
                    TicketFormate += FltHeaderList.Rows[0]["AirlinePnr"];
                    TicketFormate += "</td>";
                    TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; padding: 5px;'>Agency Info</td>";
                    TicketFormate += "<td style='font-size: 11px; width: 30%; text-align: left; padding: 5px;'>";
                    TicketFormate += FltagentDetail.Rows[0]["Mobile"];
                    TicketFormate += "<br/>";
                    TicketFormate += FltagentDetail.Rows[0]["Email"];
                    TicketFormate += "</td>";
                    TicketFormate += "</tr>";

                    TicketFormate += "<tr>";
                    TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; padding: 5px;'>Status</td>";
                    TicketFormate += "<td style='font-size: 11px; width: 30%; text-align: left; padding: 5px;'>";

                    //TicketFormate += IIf(Convert.ToString(FltHeaderList.Rows[0]["Status"]).ToLower().Trim() == "confirm" || Convert.ToString(FltHeaderList.Rows[0]["Status"]).ToLower().Trim() == "confirmbyagent", "Hold", FltHeaderList.Rows[0]["Status"]);

                    TicketFormate += (Convert.ToString(FltHeaderList.Rows[0]["Status"]).ToLower().Trim() == "confirm" || Convert.ToString(FltHeaderList.Rows[0]["Status"]).ToLower().Trim() == "confirmbyagent" ? "Hold" : FltHeaderList.Rows[0]["Status"]);
                    // TicketFormate += IIf(Convert.ToString(FltHeaderList.Rows[0]["Status"]).ToLower().Trim() = "confirm", "Hold", FltHeaderList.Rows[0]["Status"])
                    TicketFormate += "</td>";
                    TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; padding: 5px;'>Date Of Issue</td>";
                    TicketFormate += "<td style='font-size: 11px; width: 35%; text-align: left; padding: 5px;'>";
                    TicketFormate += Createddate;
                    TicketFormate += "</td>";

                    TicketFormate += "</tr>";
                    TicketFormate += "<tr>";
                    TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; padding: 5px;'></td>"; // FareType
                    TicketFormate += "<td style='font-size: 13px; width: 30%; text-align: left; padding: 5px; font-weight: bold;'>";
                    // TicketFormate += Convert.ToString(SelectedFltDS.Tables[0].Rows[0]["AdtFareType"])
                    TicketFormate += "</td>";
                    TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; padding: 5px;'>Customer Info</td>";
                    TicketFormate += "<td style='font-size: 11px; width: 30%; text-align: left; padding: 5px;'>";
                    TicketFormate += FltHeaderList.Rows[0]["PgMobile"];
                    TicketFormate += "<br/>";
                    TicketFormate += FltHeaderList.Rows[0]["PgEmail"];
                    TicketFormate += "</td>";
                    TicketFormate += "</tr>";
                    for (int p = 0; p <= FltPaxList.Rows.Count - 1; p++)
                    {
                        TicketFormate += "<tr>";
                        TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; padding: 5px;'>Passenger Name</td>";
                        TicketFormate += "<td style='font-size: 11px; width: 35%; text-align: left; padding: 5px;'>";
                        TicketFormate += FltPaxList.Rows[p]["Name"] + " " + "[" + FltPaxList.Rows[p]["PaxType"] + "]";
                        TicketFormate += "</td>";
                        TicketFormate += "<td style='font-size: 11px; width: 35%; text-align: left; padding: 5px;'>";
                        TicketFormate += FltPaxList.Rows[p]["TicketNumber"];
                        TicketFormate += "</td>";
                        TicketFormate += "</tr>";
                    }

                    TicketFormate += "</table>";
                    TicketFormate += "</td>";
                    TicketFormate += "</tr>";
                    TicketFormate += "<tr>";
                    TicketFormate += "<td style='text-align: left; background-color: #0b2759; color: #fff; width: 100%; padding: 5px;' colspan='4'>";
                    TicketFormate += "<table style='width:100%;'>";
                    TicketFormate += "<tr>";
                    TicketFormate += "<td style='text-align: left; color: #f58220; font-size: 11px; width: 25%;font-weight:bold;' colspan='1'>";
                    TicketFormate += "Flight Information";
                    TicketFormate += "</td>";
                    TicketFormate += "<td colspan='3' style='font-size: 11px; color: black; font-weight: bold; width: 75%; text-align: left; '></td>";
                    TicketFormate += "</tr>";
                    TicketFormate += "</table>";
                    TicketFormate += "</td>";
                    TicketFormate += "</tr>";

                    TicketFormate += "<tr>";
                    TicketFormate += "<td colspan='4' style='height:5px;'>&nbsp;</td>";
                    TicketFormate += "</tr>";

                    TicketFormate += "<tr>";
                    TicketFormate += "<td colspan='5' style='background-color: #0b2759;width:100%;'>";
                    TicketFormate += "<table style='width:100%;'>";
                    TicketFormate += "<tr>";
                    TicketFormate += "<td style='font-size: 10.5px; color: #fff; width: 10%; text-align: left; padding: 4px; font-weight: bold;'>FLIGHT</td>";
                    TicketFormate += "<td style='font-size: 10.5px; color: #fff; width: 20%; text-align: left; padding: 4px; font-weight: bold;'>DEPART</td>";
                    TicketFormate += "<td style='font-size: 10.5px; color: #fff; width: 20%; text-align: left; padding: 4px; font-weight: bold;'>ARRIVE</td>";
                    TicketFormate += "<td style='font-size: 10.5px; color: #fff; width: 25%; text-align: left; padding: 4px; font-weight: bold;'>DEPART AIRPORT/TERMINAL</td>";
                    TicketFormate += "<td style='font-size: 10.5px; color: #fff; width: 25%; text-align: left; padding: 4px; font-weight: bold;'>ARRIVE AIRPORT/TERMINAL</td>";
                    TicketFormate += "</tr>";

                    for (int f = 0; f <= FltDetailsList.Rows.Count - 1; f++)
                    {
                        TicketFormate += "</table>";
                        TicketFormate += "</td>";
                        TicketFormate += "</tr>";

                        TicketFormate += "<tr>";
                        TicketFormate += "<td colspan='5' style='width:100%;'>";
                        TicketFormate += "<table style='width:100%;'>";
                        TicketFormate += "<tr>";


                        TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; font-weight: bold; vertical-align: top;'>";
                        TicketFormate += FltDetailsList.Rows[f]["AirlineCode"] + " " + FltDetailsList.Rows[f]["FltNumber"];
                        TicketFormate += "<br/>";
                        TicketFormate += "<br/>";
                        TicketFormate += "<img alt='' src='http://tripforo.com/AirLogo/sm" + FltDetailsList.Rows[f]["AirlineCode"] + ".gif' ></img>";
                        // TicketFormate += "<img alt='' src='" + ResolveUrl["~/AirLogo/sm" + FltDetailsList.Rows[f]["AirlineCode"]) + ".gif' ></img>"
                        TicketFormate += "<br/>";
                        TicketFormate += "<b>" + FltDetailsList.Rows[0]["AirlineName"] + "</b>";

                        TicketFormate += "</td>";
                        TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; vertical-align: top;'>";
                        string strDepdt = Convert.ToString(FltDetailsList.Rows[f]["DepDate"]);
                        // strDepdt = strDepdt.Substring(0, 2) + "-" + strDepdt.Substring(2, 2) + "-" + strDepdt.Substring(4, 2)
                        //strDepdt = IIf(strDepdt.Length == 8, Left(strDepdt, 4) + "-" + Mid(strDepdt, 4, 2) + "-" + Right(strDepdt, 2), "20" + Right(strDepdt, 2) + "-" + Mid(strDepdt, 2, 2) + "-" + Left(strDepdt, 2));

                        strDepdt = (strDepdt.Length == 8 ? Left(strDepdt, 4) + "-" + Mid(strDepdt, 4, 2) + "-" + Right(strDepdt, 2) : "20" + Right(strDepdt, 2) + "-" + Mid(strDepdt, 2, 2) + "-" + Left(strDepdt, 2));
                        DateTime deptdt = Convert.ToDateTime(strDepdt);
                        strDepdt = deptdt.ToString("dd/MMM/yy").Replace('-', '/');

                        // 'Response.Write(strDepdt)


                        string depDay = Convert.ToString(deptdt.DayOfWeek);
                        strDepdt = strDepdt.Split('/')[0] + " " + strDepdt.Split('/')[1] + " " + strDepdt.Split('/')[2];
                        string strdeptime = Convert.ToString(FltDetailsList.Rows[f]["DepTime"]);
                        try
                        {
                            if (strdeptime.Length > 4)
                                strdeptime = strdeptime.Substring(0, 2) + " : " + strdeptime.Substring(3, 2);
                            else
                                strdeptime = strdeptime.Substring(0, 2) + " : " + strdeptime.Substring(2, 2);
                        }
                        catch (Exception ex)
                        {
                            ex.ToString();
                        }

                        TicketFormate += strDepdt;
                        TicketFormate += "<br/>";
                        TicketFormate += "<br/>";
                        TicketFormate += strdeptime;
                        TicketFormate += "</td>";

                        TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; vertical-align: top;'>";
                        string strArvdt = Convert.ToString(FltDetailsList.Rows[f]["ArrDate"]);
                        // strArvdt = strArvdt.Substring(0, 2) + "-" + strArvdt.Substring(2, 2) + "-" + strArvdt.Substring(4, 2)
                        //strArvdt = IIf(strArvdt.Length == 8, Left(strArvdt, 4) + "-" + Mid(strArvdt, 4, 2) + "-" + Right(strArvdt, 2), "20" + Right(strArvdt, 2) + "-" + Mid(strArvdt, 2, 2) + "-" + Left(strArvdt, 2));

                        strArvdt = (strArvdt.Length == 8 ? Left(strArvdt, 4) + "-" + Mid(strArvdt, 4, 2) + "-" + Right(strArvdt, 2) : "20" + Right(strArvdt, 2) + "-" + Mid(strArvdt, 2, 2) + "-" + Left(strArvdt, 2));
                        DateTime Arrdt = Convert.ToDateTime(strArvdt);
                        strArvdt = Arrdt.ToString("dd/MMM/yy").Replace('-', '/');
                        string ArrDay = Convert.ToString(Arrdt.DayOfWeek);
                        strArvdt = strArvdt.Split('/')[0] + " " + strArvdt.Split('/')[1] + " " + strArvdt.Split('/')[2];
                        string strArrtime = Convert.ToString(FltDetailsList.Rows[f]["ArrTime"]);
                        try
                        {
                            if (strArrtime.Length > 4)
                                strArrtime = strArrtime.Substring(0, 2) + " : " + strArrtime.Substring(3, 2);
                            else
                                strArrtime = strArrtime.Substring(0, 2) + " : " + strArrtime.Substring(2, 2);
                        }
                        catch (Exception ex)
                        {
                            ex.ToString();
                        }


                        TicketFormate += strArvdt;
                        TicketFormate += "<br/>";
                        TicketFormate += "<br/>";
                        TicketFormate += strArrtime;
                        TicketFormate += "</td>";
                        TicketFormate += "<td style='font-size: 11px; width: 25%; text-align: left; padding: 2px; font-weight: bold;'>";
                        TicketFormate += FltDetailsList.Rows[f]["DepAirName"] + "( " + FltDetailsList.Rows[f]["DFrom"] + "]";

                        TicketFormate += "<br />";
                        TicketFormate += "<br />";
                        //fltTerminalDetails = TerminalDetails(OrderId, FltDetailsList.Rows[f]["DFrom"].ToString(), "");
                        //// if (!String.IsNullOrEmpty(Convert.ToString(fltTerminal.Rows[0]["DepartureTerminal"])))
                        //// TicketFormate += "Terminal:" + fltTerminal.Rows[0]["DepartureTerminal"];
                        //// else
                        //if (string.IsNullOrEmpty(Convert.ToString(fltTerminalDetails.Rows[0]["DepartureTerminal"])))
                        //    TicketFormate += fltTerminalDetails.Rows[0]["DepAirportName"] + " - Trml: NA";
                        //else
                        //    TicketFormate += fltTerminalDetails.Rows[0]["DepAirportName"] + " - Trml:" + fltTerminalDetails.Rows[0]["DepartureTerminal"];

                        TicketFormate += "</td>";
                        TicketFormate += "<td style='font-size: 11px; width: 25%; text-align: left; padding: 2px; font-weight: bold;'>";
                        TicketFormate += FltDetailsList.Rows[f]["ArrAirName"] + " [" + FltDetailsList.Rows[f]["ATo"] + "]";
                        TicketFormate += "<br />";
                        TicketFormate += "<br />";
                        // if (!String.IsNullOrEmpty(Convert.ToString(fltTerminal.Rows[f]["ArrivalTerminal"])))
                        // TicketFormate += "Terminal:" + fltTerminal.Rows[f]["ArrivalTerminal"];
                        // else
                        //fltTerminalDetails = TerminalDetails(OrderId, "", FltDetailsList.Rows[f]["ATo"].ToString());
                        //if (string.IsNullOrEmpty(Convert.ToString(fltTerminalDetails.Rows[0]["ArrivalTerminal"])))
                        //    TicketFormate += fltTerminalDetails.Rows[0]["ArrvlAirportName"] + " - Trml: NA";
                        //else
                        //    TicketFormate += fltTerminalDetails.Rows[0]["ArrvlAirportName"] + " - Trml:" + fltTerminalDetails.Rows[0]["ArrivalTerminal"];

                        TicketFormate += "</td>";
                        TicketFormate += "</tr>";
                        TicketFormate += "</table>";
                        TicketFormate += "</td>";
                        TicketFormate += "</tr>";

                        TicketFormate += "<tr>";
                        TicketFormate += "<td colspan='4' style='width:100%;'>";
                        TicketFormate += "<table style='width:100%;'>";
                        TicketFormate += "<tr>";
                        TicketFormate += "<td style='font-size: 11px; width: 322%; text-align: left; font-weight:bold;'>";
                        // TicketFormate += "<img alt='Logo Not Found' src='http://flywidus.co/AirLogo/sm" + FltDetailsList.Rows[f]["AirlineCode"] + ".gif' ></img>"
                        TicketFormate += "<br/>";
                        // TicketFormate += FltDetailsList.Rows[f]["AirlineName"]
                        TicketFormate += "</td>";
                        TicketFormate += "<td style='width: 32%;'></td>";
                        TicketFormate += "<td style='width: 18%; font-size:11px;text-align:left;'></td>";
                        TicketFormate += "<td style='width: 18%; font-size: 11px; text-align: left; font-weight: bold;'></td>";
                        TicketFormate += "</tr>";
                    }
                    TicketFormate += "</table>";
                    TicketFormate += "</td>";

                    TicketFormate += "</tr>";
                    TicketFormate += "<tr>";
                    TicketFormate += "<td colspan='4' style='background-color: #0b2759; color: #f58220;font-size:11px;font-weight:bold; padding: 5px;'>";
                    TicketFormate += "Fare Information";
                    TicketFormate += "</td>";
                    TicketFormate += "</tr>";

                    if (TransID == "" || TransID == null)
                    {
                        TicketFormate += "<tr>";
                        TicketFormate += "<td colspan='8' style= 'background-color: #0b2759;width:100%;'>";
                        TicketFormate += "<table style='width:100%;'>";
                        TicketFormate += "<tr>";
                        TicketFormate += "<td style='font-size: 10px; color: #fff; width: 10%; text-align: left; padding: 4px; font-weight: bold;'>Pax Type</td>";
                        TicketFormate += "<td style='font-size: 10px; color: #fff; width: 10%; text-align: left; padding: 4px; font-weight: bold;'>Pax Count</td>";
                        TicketFormate += "<td style='font-size: 10px; color: #fff; width: 15%; text-align: left; padding: 4px; font-weight: bold;'>Base fare</td>";
                        TicketFormate += "<td style='font-size: 10px; color: #fff; width: 15%; text-align: left; padding: 4px; font-weight: bold;'>Fuel Surcharge</td>";
                        TicketFormate += "<td style='font-size: 10px; color: #fff; width: 10%; text-align: left; padding: 4px; font-weight: bold;'>Tax</td>";
                        TicketFormate += "<td style='font-size: 10px; color: #fff; width: 10%; text-align: left; padding: 4px; font-weight: bold;'>STax</td>";
                        TicketFormate += "<td style='font-size: 10px; color: #fff; width: 10%; text-align: left; padding: 4px; font-weight: bold;'>Trans Fee</td>";
                        TicketFormate += "<td style='font-size: 10px; color: #fff; width: 15%; text-align: left; padding: 4px; font-weight: bold;'>Trans Charge</td>";
                        TicketFormate += "<td style='font-size: 10px; color: #fff; width: 15%; text-align: left; padding: 4px; font-weight: bold;'>TOTAL</td>";
                        TicketFormate += "</tr>";
                        TicketFormate += "</table>";
                        TicketFormate += "</td>";
                        TicketFormate += "</tr>";


                        TicketFormate += "<tr>";
                        TicketFormate += "<td colspan='8' style='width:100%;'>";
                        TicketFormate += "<table style='width:100%;'>";
                        for (int fd = 0; fd <= fltFare.Rows.Count - 1; fd++)
                        {
                            if (fltFare.Rows[fd]["PaxType"].ToString() == "ADT" && initialAdt == 0)
                            {
                                int numberOfADT = FltPaxList.AsEnumerable().Where(x => x["PaxType"].ToString() == "ADT").ToList().Count;
                                TicketFormate += "<tr>";
                                TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; vertical-align: top;'>";
                                TicketFormate += fltFare.Rows[fd]["PaxType"];
                                TicketFormate += "</td>";
                                TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; vertical-align: top;' id='td_adtcnt'>" + numberOfADT + "</td>";
                                TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; vertical-align: top;'>";
                                TicketFormate += (Convert.ToDecimal(fltFare.Rows[fd]["BaseFare"]) * numberOfADT);
                                TicketFormate += "</td>";
                                TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; vertical-align: top;'>";
                                TicketFormate += (Convert.ToDecimal(fltFare.Rows[fd]["Fuel"]) * numberOfADT);
                                TicketFormate += "</td>";
                                TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; vertical-align: top;'id='td_taxadt'>";
                                TicketFormate += (Convert.ToDecimal(fltFare.Rows[fd]["Tax"]) * numberOfADT);
                                TicketFormate += "</td>";
                                TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; vertical-align: top;'>";
                                TicketFormate += (Convert.ToDecimal(fltFare.Rows[fd]["ServiceTax"]) * numberOfADT);
                                TicketFormate += "</td>";
                                TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; vertical-align: top;'>";
                                TicketFormate += (Convert.ToDecimal(fltFare.Rows[fd]["TFee"]) * numberOfADT);
                                TicketFormate += "</td>";
                                TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; vertical-align: top;'id='td_tcadt'>";
                                TicketFormate += (Convert.ToDecimal(fltFare.Rows[fd]["TCharge"]) * numberOfADT);
                                TicketFormate += "</td>";
                                TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; vertical-align: top;' id='td_adttot'>";
                                AdtTtlFare = (Convert.ToDecimal(fltFare.Rows[fd]["Total"]) * numberOfADT);
                                TicketFormate += AdtTtlFare.ToString();
                                TicketFormate += "</td>";

                                TicketFormate += "</tr>";

                                initialAdt += 1;
                            }

                            if (fltFare.Rows[fd]["PaxType"].ToString() == "CHD" && initalChld == 0)
                            {
                                int numberOfCHD = FltPaxList.AsEnumerable().Where(x => x["PaxType"].ToString() == "CHD").ToList().Count;
                                TicketFormate += "<tr>";
                                TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; vertical-align: top;'>";
                                TicketFormate += fltFare.Rows[fd]["PaxType"];
                                TicketFormate += "</td>";
                                TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; vertical-align: top;' id='td_chdcnt'>" + numberOfCHD + "</td>";
                                TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; vertical-align: top;'>";
                                TicketFormate += (Convert.ToDecimal(fltFare.Rows[fd]["BaseFare"]) * numberOfCHD);
                                TicketFormate += "</td>";
                                TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; vertical-align: top;'>";
                                TicketFormate += (Convert.ToDecimal(fltFare.Rows[fd]["Fuel"]) * numberOfCHD);
                                TicketFormate += "</td>";
                                TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; vertical-align: top;'id='td_taxchd'>";
                                TicketFormate += (Convert.ToDecimal(fltFare.Rows[fd]["Tax"]) * numberOfCHD);
                                TicketFormate += "</td>";
                                TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; vertical-align: top;'>";
                                TicketFormate += (Convert.ToDecimal(fltFare.Rows[fd]["ServiceTax"]) * numberOfCHD);
                                TicketFormate += "</td>";
                                TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; vertical-align: top;'>";
                                TicketFormate += (Convert.ToDecimal(fltFare.Rows[fd]["TFee"]) * numberOfCHD);
                                TicketFormate += "</td>";
                                TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; vertical-align: top;'id='td_tcchd'>";
                                TicketFormate += (Convert.ToDecimal(fltFare.Rows[fd]["TCharge"]) * numberOfCHD);
                                TicketFormate += "</td>";
                                TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; vertical-align: top;'id='td_chdtot'>";
                                ChdTtlFare = (Convert.ToDecimal(fltFare.Rows[fd]["Total"]) * numberOfCHD);
                                TicketFormate += ChdTtlFare.ToString();
                                TicketFormate += "</td>";

                                TicketFormate += "</tr>";

                                initalChld += 1;
                            }
                            if (fltFare.Rows[fd]["PaxType"].ToString() == "INF" && initialift == 0)
                            {
                                int numberOfINF = FltPaxList.AsEnumerable().Where(x => x["PaxType"].ToString() == "INF").ToList().Count;
                                TicketFormate += "<tr>";
                                TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; vertical-align: top;'>";
                                TicketFormate += fltFare.Rows[fd]["PaxType"];
                                TicketFormate += "</td>";
                                TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; vertical-align: top;' id='td_infcnt'>" + numberOfINF + "</td>";
                                TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; vertical-align: top;'>";
                                TicketFormate += (Convert.ToDecimal(fltFare.Rows[fd]["BaseFare"]) * numberOfINF);
                                TicketFormate += "</td>";
                                TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; vertical-align: top;'>";
                                TicketFormate += (Convert.ToDecimal(fltFare.Rows[fd]["Fuel"]) * numberOfINF);
                                TicketFormate += "</td>";
                                TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; vertical-align: top;'>";
                                TicketFormate += (Convert.ToDecimal(fltFare.Rows[fd]["Tax"]) * numberOfINF);
                                TicketFormate += "</td>";
                                TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; vertical-align: top;'>";
                                TicketFormate += (Convert.ToDecimal(fltFare.Rows[fd]["ServiceTax"]) * numberOfINF);
                                TicketFormate += "</td>";
                                TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; vertical-align: top;'>";
                                TicketFormate += (Convert.ToDecimal(fltFare.Rows[fd]["TFee"]) * numberOfINF);
                                TicketFormate += "</td>";
                                TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; vertical-align: top;'>";
                                TicketFormate += (Convert.ToDecimal(fltFare.Rows[fd]["TCharge"]) * numberOfINF);
                                TicketFormate += "</td>";
                                TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; vertical-align: top;'id='td_Inftot'>";
                                INFTtlFare = (Convert.ToDecimal(fltFare.Rows[fd]["Total"]) * numberOfINF);
                                TicketFormate += INFTtlFare.ToString();
                                TicketFormate += "</td>";
                                TicketFormate += "</tr>";
                                initialift += 1;
                            }
                        }
                        fare = AdtTtlFare + ChdTtlFare + INFTtlFare;
                    }
                    else
                    {
                        TicketFormate += "<tr>";
                        TicketFormate += "<td colspan='2' style='width:100%;'>";
                        TicketFormate += "<table style='width:100%;'>";


                        TicketFormate += "<tr>";
                        TicketFormate += "<td style='font-size: 10px; width: 50%; text-align: left; vertical-align: top; display:none;'id='td_perpaxtype'>" + FltPaxList.Rows[0]["PaxType"] + "</td>";
                        TicketFormate += "<td style='font-size: 10px; width: 50%; text-align: left; vertical-align: top;'>Base Fare</td>";
                        TicketFormate += "<td style='font-size: 10px; width: 50%; text-align: left; vertical-align: top;'>";
                        TicketFormate += fltFare.Rows[0]["BaseFare"];
                        TicketFormate += "</td>";
                        TicketFormate += "</tr>";
                        TicketFormate += "<tr>";
                        TicketFormate += "<td style='font-size: 10px; width: 50%; text-align: left; vertical-align: top;'>Fuel Surcharge</td>";
                        TicketFormate += "<td style='font-size: 10px; width: 50%; text-align: left; vertical-align: top;'>";
                        TicketFormate += fltFare.Rows[0]["Fuel"];
                        TicketFormate += "</td>";
                        TicketFormate += "</tr>";
                        TicketFormate += "<tr>";
                        TicketFormate += "<td style='font-size: 10px; width: 50%; text-align: left; vertical-align: top;'>Tax</td>";
                        TicketFormate += "<td style='font-size: 10px; width: 50%; text-align: left; vertical-align: top;' id='td_perpaxtax'>";
                        TicketFormate += fltFare.Rows[0]["Tax"];
                        TicketFormate += "</td>";
                        TicketFormate += "</tr>";
                        TicketFormate += "<tr>";
                        TicketFormate += "<td style='font-size: 10px; width: 50%; text-align: left; vertical-align: top;'>STax</td>";
                        TicketFormate += "<td style='font-size: 10px; width: 50%; text-align: left; vertical-align: top;'>";
                        TicketFormate += fltFare.Rows[0]["ServiceTax"];
                        TicketFormate += "</td>";
                        TicketFormate += "</tr>";
                        TicketFormate += "<tr>";
                        TicketFormate += "<td style='font-size: 10px; width: 50%; text-align: left; vertical-align: top;'>Trans Fee</td>";
                        TicketFormate += "<td style='font-size: 10px; width: 50%; text-align: left; vertical-align: top;'>";
                        TicketFormate += fltFare.Rows[0]["TFee"];
                        TicketFormate += "</td>";
                        TicketFormate += "</tr>";
                        TicketFormate += "<tr>";
                        TicketFormate += "<td style='font-size: 10px; width: 50%; text-align: left; vertical-align: top;'>Trans Charge</td>";
                        TicketFormate += "<td style='font-size: 10px; width: 50%; text-align: left; vertical-align: top;'id='td_perpaxtc'>";
                        TicketFormate += fltFare.Rows[0]["TCharge"];
                        TicketFormate += "</td>";
                        TicketFormate += "</tr>";
                        TicketFormate += "<tr>";

                        decimal ResuCharge = 0;
                        decimal ResuServiseCharge = 0;
                        decimal ResuFareDiff = 0;
                        decimal srvChargeamt = 0;
                        decimal proxychagreamt = 0;
                        decimal importchagreamt = 0;
                        if (Convert.ToString(FltHeaderList.Rows[0]["ResuCharge"]) != null && Convert.ToString(FltHeaderList.Rows[0]["ResuCharge"]) != "")
                        {
                            TicketFormate += "<tr>";
                            TicketFormate += "<td style='font-size: 10px; width: 50%; text-align: left; vertical-align: top;'>Reissue Charge</td>";
                            TicketFormate += "<td style='font-size: 10px; width: 50%; text-align: left; vertical-align: top;'>";
                            TicketFormate += FltHeaderList.Rows[0]["ResuCharge"];
                            ResuCharge = (Convert.ToDecimal(FltHeaderList.Rows[0]["ResuCharge"]));
                            TicketFormate += "</td>";
                            TicketFormate += "</tr>";
                        }
                        if (Convert.ToString(FltHeaderList.Rows[0]["ResuServiseCharge"]) != null && Convert.ToString(FltHeaderList.Rows[0]["ResuServiseCharge"]) != "")
                        {
                            TicketFormate += "<tr>";
                            TicketFormate += "<td style='font-size: 10px; width: 50%; text-align: left; vertical-align: top;'>Reissue Srv. Charge</td>";
                            TicketFormate += "<td style='font-size: 10px; width: 50%; text-align: left; vertical-align: top;'>";
                            TicketFormate += FltHeaderList.Rows[0]["ResuServiseCharge"];
                            ResuServiseCharge = (Convert.ToDecimal(FltHeaderList.Rows[0]["ResuServiseCharge"]));
                            TicketFormate += "</td>";
                            TicketFormate += "</tr>";
                        }
                        if (Convert.ToString(FltHeaderList.Rows[0]["ResuFareDiff"]) != null && Convert.ToString(FltHeaderList.Rows[0]["ResuFareDiff"]) != "")
                        {
                            TicketFormate += "<tr>";
                            TicketFormate += "<td style='font-size: 10px; width: 50%; text-align: left; vertical-align: top;'>Reissue Fare Diff</td>";
                            TicketFormate += "<td style='font-size: 10px; width: 50%; text-align: left; vertical-align: top;'>";
                            TicketFormate += FltHeaderList.Rows[0]["ResuFareDiff"];
                            ResuFareDiff = (Convert.ToDecimal(FltHeaderList.Rows[0]["ResuFareDiff"]));
                            TicketFormate += "</td>";
                            TicketFormate += "</tr>";
                        }


                        if (Convert.ToString(FltHeaderList.Rows[0]["Import_Charge"]) != null)
                        {
                            TicketFormate += "<tr>";
                            TicketFormate += "<td style='font-size: 10px; width: 50%; text-align: left; vertical-align: top;'>Import Charge</td>";
                            TicketFormate += "<td style='font-size: 10px; width: 50%; text-align: left; vertical-align: top;'>";
                            TicketFormate += FltHeaderList.Rows[0]["Import_Charge"];
                            importchagreamt = (Convert.ToDecimal(FltHeaderList.Rows[0]["Import_Charge"]));
                            TicketFormate += "</td>";
                            TicketFormate += "</tr>";
                        }

                        // 'add by m on 7-03-18
                        if ((ProxyCharge.Rows.Count > 0))
                        {

                            if (Convert.ToString(ProxyCharge.Rows[0]["ProxyCharge"]) != null && Convert.ToString(ProxyCharge.Rows[0]["ProxyCharge"]) != "")
                            {
                                TicketFormate += "<tr>";
                                TicketFormate += "<td style='font-size: 10px; width: 50%; text-align: left; vertical-align: top;'>Proxy Charge</td>";
                                TicketFormate += "<td style='font-size: 10px; width: 50%; text-align: left; vertical-align: top;'>";
                                TicketFormate += ProxyCharge.Rows[0]["ProxyCharge"];
                                proxychagreamt = (Convert.ToDecimal(ProxyCharge.Rows[0]["ProxyCharge"]));
                                TicketFormate += "</td>";
                                TicketFormate += "</tr>";
                            }
                        }




                        TicketFormate += "<td style='font-size: 11px; width: 50%; text-align: left; vertical-align: top;'>TOTAL</td>";
                        TicketFormate += "<td style='font-size: 11px; width: 50%; text-align: left; vertical-align: top;' id='td_totalfare'>";
                        fare = (Convert.ToDecimal(fltFare.Rows[0]["BaseFare"]) + Convert.ToDecimal(fltFare.Rows[0]["Fuel"]) + Convert.ToDecimal(fltFare.Rows[0]["Tax"]) + Convert.ToDecimal(fltFare.Rows[0]["ServiceTax"]) + Convert.ToDecimal(fltFare.Rows[0]["TCharge"]) + Convert.ToDecimal(fltFare.Rows[0]["TFee"]) + ResuCharge + ResuServiseCharge + ResuFareDiff + srvChargeamt + proxychagreamt + importchagreamt);
                        TicketFormate += fare.ToString();
                        TicketFormate += "</td>";
                        TicketFormate += "</tr>";
                    }
                    TicketFormate += "</table>";
                    TicketFormate += "</td>";
                    TicketFormate += "</tr>";
                    if (fltMealAndBag.Rows.Count > 0)
                    {
                        TicketFormate += "<tr>";
                        TicketFormate += "<td colspan='7' style= 'background-color: #0b2759;width:100%;'>";
                        TicketFormate += "<table style='width:100%;'>";
                        TicketFormate += "<tr>";
                        TicketFormate += "<td style='font-size: 10px; color: #fff; width: 15%; text-align: left; padding: 4px; font-weight: bold;'>Pax Name</td>";
                        TicketFormate += "<td style='font-size: 10px; color: #fff; width: 15%; text-align: left; padding: 4px; font-weight: bold;'>Trip Type</td>";
                        TicketFormate += "<td style='font-size: 10px; color: #fff; width: 15%; text-align: left; padding: 4px; font-weight: bold;'>Meal Code</td>";
                        TicketFormate += "<td style='font-size: 10px; color: #fff; width: 15%; text-align: left; padding: 4px; font-weight: bold;'>Meal Price</td>";
                        TicketFormate += "<td style='font-size: 10px; color: #fff; width: 15%; text-align: left; padding: 4px; font-weight: bold;'>Baggage Code</td>";
                        TicketFormate += "<td style='font-size: 10px; color: #fff; width: 15%; text-align: left; padding: 4px; font-weight: bold;'>Baggage Price</td>";
                        TicketFormate += "<td style='font-size: 10px; color: #fff; width: 10%; text-align: left; padding: 4px; font-weight: bold;'>TOTAL</td>";
                        TicketFormate += "</tr>";
                        TicketFormate += "</table>";
                        TicketFormate += "</td>";
                        TicketFormate += "</tr>";

                        TicketFormate += "<tr>";
                        TicketFormate += "<td colspan='7' style='width:100%;'>";
                        TicketFormate += "<table style='width:100%;'>";


                        for (int i = 0; i <= fltMealAndBag.Rows.Count - 1; i++)
                        {
                            // If Convert.ToString(fltMealAndBag.Rows[i]["MealPrice"]) <> "0.00" AndAlso Convert.ToString(fltMealAndBag.Rows[i]["BaggagePrice"]) <> "0.00" Then
                            TicketFormate += "<tr>";
                            TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; vertical-align: top;'>";
                            TicketFormate += Convert.ToString(fltMealAndBag.Rows[i]["Name"]);
                            TicketFormate += "</td>";
                            TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; vertical-align: top;'>";
                            TicketFormate += Convert.ToString(fltMealAndBag.Rows[i]["TripType"]);
                            TicketFormate += "</td>";
                            TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; vertical-align: top;'>";
                            TicketFormate += Convert.ToString(fltMealAndBag.Rows[i]["MealCode"]);
                            TicketFormate += "</td>";
                            TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; vertical-align: top;'>";
                            TicketFormate += Convert.ToString(fltMealAndBag.Rows[i]["MealPrice"]);
                            TicketFormate += "</td>";
                            TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; vertical-align: top;'>";
                            TicketFormate += Convert.ToString(fltMealAndBag.Rows[i]["BaggageCode"]);
                            TicketFormate += "</td>";
                            TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; vertical-align: top;'>";
                            TicketFormate += Convert.ToString(fltMealAndBag.Rows[i]["BaggagePrice"]);
                            TicketFormate += "</td>";
                            TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; vertical-align: top;'>";
                            MealBagTotalPrice += Convert.ToDecimal(fltMealAndBag.Rows[i]["TotalPrice"]);
                            TicketFormate += Convert.ToString(fltMealAndBag.Rows[i]["TotalPrice"]);
                            TicketFormate += "</td>";

                            TicketFormate += "</tr>";
                        }
                        TicketFormate += "</table>";
                        TicketFormate += "</td>";
                        TicketFormate += "</tr>";
                    }



                    TicketFormate += "<tr>";
                    TicketFormate += "<td colspan='4' style='background-color: #0b2759; color:#fdc42c;font-size:11px;font-weight:bold; padding: 5px;'>";
                    TicketFormate += "<table style='width:100%;'>";
                    TicketFormate += "<tr>";
                    TicketFormate += "<td style='font-size: 10px; width: 20%; text-align: left; vertical-align: top;'></td>";
                    TicketFormate += "<td style='font-size: 10px; width: 20%; text-align: left; vertical-align: top;'></td>";
                    TicketFormate += "<td style='font-size: 10px; width: 20%; text-align: left; vertical-align: top;'></td>";
                    TicketFormate += "<td style='font-size: 10px; width: 15%; text-align: left; vertical-align: top;'></td>";
                    TicketFormate += "<td style='color: #fff; font-size: 10px; width: 15%; text-align: left; vertical-align: top;'>GRAND TOTAL</td>";
                    TicketFormate += "<td style='color: #fff; font-size: 10px; width: 10%; text-align: left; vertical-align: top;'id='td_grandtot'>";
                    TicketFormate += (fare + MealBagTotalPrice).ToString();
                    TicketFormate += "</td>";

                    TicketFormate += "</tr>";
                    TicketFormate += "</table>";
                    TicketFormate += "</td>";
                    TicketFormate += "</tr>";

                    TicketFormate += "<br/><br/>";
                    TicketFormate += "<tr>";
                    TicketFormate += "<td colspan='4'>";
                    TicketFormate += "<ul style='list-style-image: url(http://tripforo.com/Images/bullet.png);'>";
                    TicketFormate += "<li style='font-size:10.5px;'>Kindly confirm the status of your PNR within 24 hrs of booking, as at times the same may fail on account of payment failure, internet connectivity, booking engine or due to any other reason beyond our control.";
                    TicketFormate += "For Customers who book their flights well in advance of the scheduled departure date it is necessary that you re-confirm the departure time of your flight between 72 and 24 hours before the Scheduled Departure Time.</li>";
                    TicketFormate += "</ul>";
                    TicketFormate += "</td>";
                    TicketFormate += "</tr>";

                    TicketFormate += "<tr>";
                    TicketFormate += "<td colspan='4' style='background-color: #0b2759; color: #f58220; font-size: 11px; font-weight: bold; padding: 5px;'>TERMS AND CONDITIONS :</td>";
                    TicketFormate += "</tr>";

                    TicketFormate += "<tr>";
                    TicketFormate += "<td colspan='4'>";
                    TicketFormate += "<ul style='list-style-image: url(http://tripforo.com/Images/bullet.png);'>";
                    TicketFormate += "<li style='font-size:10.5px;'>Guests are requested to carry their valid photo identification for all guests, including children.</li>";
                    TicketFormate += "<li style='font-size:10.5px;'>We recommend check-in at least 2 hours prior to departure.</li>";
                    TicketFormate += "<li style='font-size:10.5px;'>Boarding gates close 45 minutes prior to the scheduled time of departure. Please report at your departure gate at the indicated boarding time. Any passenger failing to report in time may be refused boarding privileges.</li>";
                    TicketFormate += "<li style='font-size:10.5px;'>Cancellations and Changes permitted more than two [2] hours prior to departure with payment of change fee and difference in fare if applicable only in working hours (10:00 am to 06:00 pm) except Sundays and Holidays.</li>";
                    TicketFormate += "<li style='font-size:10.5px;'>";
                    TicketFormate += "Flight schedules are subject to change and approval by authorities.";
                    TicketFormate += "<br />";
                    TicketFormate += "</li>";
                    TicketFormate += "<li style='font-size:10.5px;'>";
                    TicketFormate += "Name Changes on a confirmed booking are strictly prohibited. Please ensure that the name given at the time of booking matches as mentioned on the traveling Guests valid photo ID Proof.";
                    TicketFormate += "<br />";
                    TicketFormate += " Travel Agent does not provide compensation for travel on other airlines, meals, lodging or ground transportation.";
                    TicketFormate += "</li>";
                    TicketFormate += "<li style='font-size:10.5px;'>Bookings made under the Armed Forces quota are non cancelable and non- changeable.</li>";

                    TicketFormate += "<li style='font-size:10.5px;'>Guests are advised to check their all flight details (including their Name, Flight numbers, Date of Departure, Sectors) before leaving the Agent Counter.</li>";
                    TicketFormate += "<li style='font-size:10.5px;'>Cancellation amount will be charged as per airline rule.</li>";
                    TicketFormate += "<li style='font-size:10.5px;'>Guests requiring wheelchair assistance, stretcher, Guests traveling with infants and unaccompanied minors need to be booked in advance since the inventory for these special service requests are limited per flight.</li>";
                    TicketFormate += "</ul>";
                    TicketFormate += "</td>";
                    TicketFormate += "</tr>";
                    TicketFormate += "</table>";
                    TicketFormate += "<table style='width: 100%;'>";
                    TicketFormate += "<tr>";
                    TicketFormate += "<td colspan='4' style='background-color: #0b2759; color: #f58220; font-size: 11px; font-weight: bold; padding: 5px;'>BAGGAGE INFORMATION :";
                    TicketFormate += "</td>";
                    TicketFormate += "</tr>";

                    if (!string.IsNullOrEmpty(Convert.ToString(SelectedFltDS.Tables[0].Rows[0]["IsBagFare"])))
                        Bag = Convert.ToBoolean(SelectedFltDS.Tables[0].Rows[0]["IsBagFare"]);

                    DataTable dtbaggage = new DataTable();
                    dtbaggage = GetBaggageInformation(Convert.ToString(FltHeaderList.Rows[0]["Trip"]), FltHeaderList.Rows[0]["VC"].ToString(), Bag).Tables[0];
                    string bginfo = GetBagInfo(Convert.ToString(SelectedFltDS.Tables[0].Rows[0]["Provider"]), Convert.ToString(SelectedFltDS.Tables[0].Rows[0]["AirlineRemark"]));

                    if (bginfo == "")
                    {
                        //foreach (var drbg in dtbaggage.Rows)
                        //{
                        for (int i = 0; i < dtbaggage.Rows.Count; i++)
                        {
                            TicketFormate += "<tr>";
                            TicketFormate += "<td colspan='2'>" + dtbaggage.Rows[i]["BaggageName"].ToString() + "</td>";
                            TicketFormate += "<td colspan='2'>" + dtbaggage.Rows[i]["Weight"].ToString() + "</td>";
                            TicketFormate += "</tr>";
                        }
                    }
                    else
                    {
                        TicketFormate += "<tr>";
                        TicketFormate += "<td colspan='2'></td>";
                        TicketFormate += "<td colspan='2'>" + bginfo + "</td>";
                        TicketFormate += "</tr>";
                    }




                    TicketFormate += "</table>";
                }


                return TicketFormate;
            }

            catch (Exception ex)
            {
                ex.ToString();
            }
            return TicketFormate;
        }


        public static DataTable SelectHeaderDetail(string OrderId)
        {
            SqlConnection con = new SqlConnection(GetConnection);
            SqlDataAdapter adap;
            DataTable dt = new DataTable();
            try
            {
                adap = new SqlDataAdapter("SELECT isnull(ImportCharge,0) as Import_Charge,*  FROM  FltHeader WHERE OrderId = '" + OrderId + "' ", con);
                adap.Fill(dt);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }


            return dt;
        }

        public static DataTable SelectFlightDetail(string OrderId)
        {
            SqlConnection con = new SqlConnection(GetConnection);
            SqlDataAdapter adap;
            DataTable dt = new DataTable();
            adap = new SqlDataAdapter("SELECT FltId, OrderId, DepCityOrAirportCode AS DFrom, DepCityOrAirportName as DepAirName, ArrCityOrAirportCode AS ATo, ArrCityOrAirportName as ArrAirName, DepDate, DepTime, ArrDate, ArrTime, AirlineCode, AirlineName, FltNumber, AirCraft, CreateDate, UpdateDate,ISNULL(AdtRbd,'') as AdtRbd,ISNULL(ChdRbd,'') as ChdRbd FROM FltDetails  WHERE OrderId = '" + OrderId + "' Order by FltId ", con);
            adap.Fill(dt);
            return dt;
        }


        public static string GetBagInfo(string Provider, string Remark)
        {
            string baginfo = "";
            if (Provider == "TB")
            {
                if (Remark.Contains("Hand"))
                    baginfo = Remark;
            }
            else if (Provider == "YA")
            {
                if (Remark.Contains("Hand"))
                    baginfo = Remark;
                else if (!string.IsNullOrEmpty(Remark))
                    baginfo = Remark + " Baggage allowance";
            }
            else if (Provider == "1G")
            {
                if (Remark.Contains("PC"))
                    baginfo = Remark.Replace("PC", " Piece(s) Baggage allowance");
                else if (Remark.Contains("K"))
                    baginfo = Remark.Replace("K", " Kg Baggage allowance");
            }
            return baginfo;
        }

        public static DataTable AgentIDInfo(string AgentID)
        {
            SqlConnection con = new SqlConnection(GetConnection);
            SqlDataAdapter adap;
            DataTable dt = new DataTable();
            adap = new SqlDataAdapter("select * from agent_register where user_id='" + AgentID + "'", con);
            adap.Fill(dt);
            return dt;
        }

        public static DataTable SelectPaxDetail(string OrderId, string TID)
        {
            SqlConnection con = new SqlConnection(GetConnection);
            SqlDataAdapter adap = new SqlDataAdapter();
            if (string.IsNullOrEmpty(TID))
            {
                DataTable dt = new DataTable();

                adap = new SqlDataAdapter("SELECT PaxId, OrderId, Title + '  ' + FName + '  ' + MName + '  ' + LName AS Name, PaxType, TicketNumber,DOB,FFNumber,FFAirline,MealType,SeatType FROM   FltPaxDetails WHERE OrderId = '" + OrderId + "' ", con);
                // adap = New SqlDataAdapter["SELECT PaxId, OrderId, Title + '  ' + FName + '  ' + MName + '  ' + LName AS Name, PaxType, TicketNumber,DOB FROM   FltPaxDetails WHERE OrderId = '" & OrderId & "' ", con)
                adap.Fill(dt);

                return dt;
            }
            else
            {
                DataTable dt = new DataTable();
                adap = new SqlDataAdapter("SELECT PaxId, OrderId, PaxId, Title + '  ' + FName + '  ' + MName + '  ' + LName AS Name, PaxType, TicketNumber,DOB,FFNumber,FFAirline,MealType,SeatType FROM   FltPaxDetails WHERE OrderId = '" + OrderId + "' and PaxId= '" + TID + "' ", con);
                // adap = New SqlDataAdapter["SELECT PaxId, OrderId, PaxId, Title + '  ' + FName + '  ' + MName + '  ' + LName AS Name, PaxType, TicketNumber,DOB FROM   FltPaxDetails WHERE OrderId = '" & OrderId & "' and PaxId= '" & TID & "' ", con)
                adap.Fill(dt);
                return dt;
            }
        }

        public static DataTable TerminalDetails(string OrderID, string DepCity, string ArrvlCity)
        {
            SqlConnection con = new SqlConnection(GetConnection);
            SqlDataAdapter adap = new SqlDataAdapter("USP_TERMINAL_INFO", con);
            adap.SelectCommand.CommandType = CommandType.StoredProcedure;
            adap.SelectCommand.Parameters.AddWithValue("@DEPARTURECITY", DepCity);
            adap.SelectCommand.Parameters.AddWithValue("@ARRIVALCITY", ArrvlCity);
            adap.SelectCommand.Parameters.AddWithValue("@ORDERID", OrderID);
            DataTable dt1 = new DataTable();
            con.Open();
            adap.Fill(dt1);
            con.Close();
            return dt1;
        }

        public static DataTable ProxyChargeMth(string OrderID)
        {
            SqlConnection con = new SqlConnection(GetConnection);
            SqlDataAdapter adap = new SqlDataAdapter("Proxy_chargeOnTicket", con);
            adap.SelectCommand.CommandType = CommandType.StoredProcedure;
            adap.SelectCommand.Parameters.AddWithValue("@OrderID", OrderID);
            DataTable dt1 = new DataTable();
            con.Open();
            adap.Fill(dt1);
            con.Close();
            return dt1;
        }

        public static DataTable SelectAgent(string OrderId)
        {
            SqlConnection con = new SqlConnection(GetConnection);
            SqlDataAdapter adap;
            DataTable dt = new DataTable();
            adap = new SqlDataAdapter("SELECT AgentId FROM   FltHeader WHERE OrderId = '" + OrderId + "' ", con);
            adap.Fill(dt);
            return dt;
        }

        public static DataSet GetFltDtls(string orderid, string agentid)
        {
            DataSet dataSet = new DataSet();
            DataTable dataTable = new DataTable();
            try
            {

                SqlDataAdapter sqlDataAdapter;
                SqlConnection con = new SqlConnection(GetConnection);
                SqlCommand sqlCommand = new SqlCommand("GetSelectedFltDtls_Gal", con);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@TrackId", orderid));
                sqlCommand.Parameters.Add(new SqlParameter("@UserId", agentid));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "GetSelectedFltDtls");
                dataTable = dataSet.Tables["GetSelectedFltDtls"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataSet;
        }

        public static DataSet GetBaggageInformation(string Trip, string VS, bool IsBagFare)
        {
            DataSet dataSet = new DataSet();
            DataTable dataTable = new DataTable();
            try
            {
                SqlDataAdapter sqlDataAdapter;
                SqlConnection con = new SqlConnection(GetConnection);
                SqlCommand sqlCommand = new SqlCommand("SP_GETBAGGAGE_INFO", con);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@TRIP", Trip));
                sqlCommand.Parameters.Add(new SqlParameter("@AIRLINE", VS));
                sqlCommand.Parameters.Add(new SqlParameter("@IsBagFare", IsBagFare));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "GETBAGGAGE");
                dataTable = dataSet.Tables["GETBAGGAGE"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataSet;
        }

        public static DataSet GetMailingDetails(string department, string UserID)
        {
            DataSet dataSet = new DataSet();
            DataTable dataTable = new DataTable();
            try
            {
                SqlDataAdapter sqlDataAdapter;
                SqlConnection con = new SqlConnection(GetConnection);
                SqlCommand sqlCommand = new SqlCommand("SP_GETMAILINGCREDENTIAL_ITZ", con);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@Department", department));
                sqlCommand.Parameters.Add(new SqlParameter("@UserID", UserID));               
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "SP_GETMAILINGCREDENTIAL");
                dataTable = dataSet.Tables["SP_GETMAILINGCREDENTIAL"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataSet;
        }

        public static DataTable SelectAgencyDetail(string AgentID)
        {
            SqlConnection con = new SqlConnection(GetConnection);
            SqlDataAdapter adap;
            DataTable dt = new DataTable();
            adap = new SqlDataAdapter("SELECT Agency_Name,Email,Mobile,Address,Address,(City+' - '+State+' - '+Country) as Address1,IsCorp,ag_logo from agent_register  WHERE User_Id = '" + AgentID + "' ", con);
            adap.Fill(dt);
            return dt;
        }

        public static DataTable SelectFareDetail(string OrderId, string TID)
        {
            SqlConnection con = new SqlConnection(GetConnection);
            SqlDataAdapter adap;
            if (string.IsNullOrEmpty(TID))
            {
                DataTable dt = new DataTable();
                adap = new SqlDataAdapter("select PaxType, BaseFare, YQ as Fuel,  (YR+WO+OT+K3) as Tax,ServiceTax,TranFee as TFee, (AdminMrk+AgentMrk+DistrMrk) as TCharge,(BaseFare+YQ+YR+WO+OT+ServiceTax+AdminMrk+AgentMrk+DistrMrk+TranFee+K3) as Total,TotalAfterDis , ISNULL(MgtFee,0) as MgtFee,ISNULL(FareType,'') as FareType,ISNULL(TotalDiscount,0) AS Discount,isnull(Tds,0) as tds  FROM FltFareDetails where OrderId='" + OrderId + "' ", con);
                adap.Fill(dt);
                return dt;
            }
            else
            {
                DataTable dt = new DataTable();
                adap = new SqlDataAdapter("SELECT  FltFareDetails.BaseFare as BaseFare, FltFareDetails.YQ as Fuel,  (FltFareDetails.YR+FltFareDetails.WO+FltFareDetails.OT+FltFareDetails.K3) as Tax,FltFareDetails.ServiceTax as ServiceTax,FltFareDetails.TranFee as TFee, FltFareDetails.AdminMrk+FltFareDetails.AgentMrk+FltFareDetails.DistrMrk as TCharge,TotalAfterDis , ISNULL(MgtFee,0) as MgtFee,ISNULL(FareType,'') as FareType,ISNULL(FltFareDetails.TotalDiscount,0) AS Discount,isnull(FltFareDetails.Tds,0) as tds  FROM FltPaxDetails INNER JOIN FltFareDetails ON FltPaxDetails.OrderId = FltFareDetails.OrderId AND FltPaxDetails.PaxType = FltFareDetails.PaxType WHERE FltPaxDetails.PaxId = '" + TID + "' ", con);
                adap.Fill(dt);
                return dt;
            }
        }

        public static string Left(string param, int length)
        {
            //we start at 0 since we want to get the characters starting from the
            //left and with the specified lenght and assign it to a variable
            string result = param.Substring(0, length);
            //return the result of the operation
            return result;
        }
        public static string Right(string param, int length)
        {
            //start at the index based on the lenght of the sting minus
            //the specified lenght and assign it a variable
            string result = param.Substring(param.Length - length, length);
            //return the result of the operation
            return result;
        }

        public static string Mid(string param, int startIndex, int endIndex)
        {
            //start at the specified index in the string ang get N number of
            //characters depending on the lenght and assign it to a variable
            string result = param.Substring(startIndex, endIndex);
            //return the result of the operation
            return result;
        }


        public static int SendMail(string toEMail, string from, string bcc, string cc, string smtpClient, string userID, string pass, string body, string subject, string AttachmentFile)
        {
            System.Net.Mail.SmtpClient objMail = new System.Net.Mail.SmtpClient();
            System.Net.Mail.MailMessage msgMail = new System.Net.Mail.MailMessage();
            msgMail.To.Clear();
            msgMail.To.Add(new System.Net.Mail.MailAddress(toEMail));
            msgMail.From = new System.Net.Mail.MailAddress(from);
            if (bcc != "")
                msgMail.Bcc.Add(new System.Net.Mail.MailAddress(bcc));
            if (cc != "")
                msgMail.CC.Add(new System.Net.Mail.MailAddress(cc));
            if (AttachmentFile != "")
                msgMail.Attachments.Add(new System.Net.Mail.Attachment(AttachmentFile));

            msgMail.Subject = subject;
            msgMail.IsBodyHtml = true;
            msgMail.Body = body;


            try
            {
                objMail.Credentials = new System.Net.NetworkCredential(userID, pass);
                objMail.EnableSsl = true;
                objMail.Host = smtpClient;
                objMail.Send(msgMail);
                return 1;
            }
            catch (Exception ex)
            {
                ex.ToString();
                return 0;
            }
        }



    }
}