﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixedDD.Models
{
    public class Account
    {
        public string UserId { get; set; }
        public string Password { get; set; }
    }
    public class MarkupModel
    {
        public string Airline { get; set; }

        public string FlightNo { get; set; }
        public string Counter { get; set; }
        public string Trip { get; set; }
        public string AgentId { get; set; }
        public string Amount { get; set; }
        public string GroupType { get; set; }
        public string Org { get; set; }
        public string Dest { get; set; }
        public string createdDate { get; set; }
        public string CommisionOnBasic { get; set; }
        public string CommissionOnYq { get; set; }
        public string CommisionOnBasicYq { get; set; }
        public string MarkupType { get; set; }
        public string FareType { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public int FddId { get; set; }
    }
    public class FixdDepModel
    {
        public string Id { get; set; }
        public string Fare { get; set; }
        public string Grand_total { get; set; }
        public string Seat { get; set; }
        public string Avl_Seat { get; set; }
        public string Amount { get; set; }
        public string Basefare { get; set; }
        public string YQ { get; set; }
        public string YR { get; set; }
        public string OT { get; set; }
        public string WO { get; set; }
        public string Markup { get; set; }
    }

    public class FixdDepHoldBookingModel
    {
        public string OrderId { get; set; }
        public string Remark { get; set; }
        public string GdsPnr { get; set; }
        public string AgentId { get; set; }
        public string totalcost { get; set; }
    }
    public class FlightTicketReportModel
    {
        public int HeaderId { get; set; }
        public string sector { get; set; }

        public string Suppliorid { get; set; }
        public string Status { get; set; }
        public string JourneyDate { get; set; }
        public string PaxType { get; set; }
        public int Totalcount { get; set; }
        public string Provider { get; set; }
        public string PName { get; set; }
        public string MordifyStatus { get; set; }
        public string GdsPnr { get; set; }
        public string AirlinePnr { get; set; }
        public string VC { get; set; }
        public string Duration { get; set; }
        public string TripType { get; set; }
        public string TourCode { get; set; }
        public decimal TotalBookingCost { get; set; }
        public decimal TotalAfterDis { get; set; }
        public decimal SFDis { get; set; }
        public decimal AdditionalMarkup { get; set; }
        public int Adult { get; set; }
        public int Child { get; set; }
        public int Infant { get; set; }
        public string AgentId { get; set; }
        public string AgencyName { get; set; }
        public string AgentType { get; set; }
        public string DistrId { get; set; }
        public string ExecutiveId { get; set; }
        public string PaymentType { get; set; }
        public string PgTitle { get; set; }
        public string PgFName { get; set; }
        public string PgLName { get; set; }
        public string PgMobile { get; set; }
        public string PgEmail { get; set; }
        public string Remark { get; set; }
        public string RejectedRemark { get; set; }
        public string CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public string ResuID { get; set; }
        public decimal ResuCharge { get; set; }
        public decimal ResuServiseCharge { get; set; }
        public decimal ResuFareDiff { get; set; }
        public int PaxId { get; set; }
        public decimal ImportCharge { get; set; }
        public bool YFLAG { get; set; }
        public bool YCRN { get; set; }
        public bool Y_CAN_FARE { get; set; }
        public string ProjectID { get; set; }
        public string BillNoCorp { get; set; }
        public string BookedBy { get; set; }
        public bool IsMobile { get; set; }
        public string FareType { get; set; }
        public string ReferenceNo { get; set; }
        public string APIID { get; set; }
        public string PartnerName { get; set; }
        public string PreHoldREmark { get; set; }
        public string PreHoldUpdatedBy { get; set; }
        public DateTime PreholdUpdateDate { get; set; }
        public string PaymentMode { get; set; }
        public string PgTid { get; set; }
        public string PgCharges { get; set; }
        public string FareRule { get; set; }
        public bool ReIssueRefundStatus { get; set; }
        public bool MSent { get; set; }
        public string GSTNO { get; set; }
        public string GST_Company_Name { get; set; }
        public string GST_Company_Address { get; set; }
        public string GST_PhoneNo { get; set; }
        public string GST_Email { get; set; }
        public string GSTRemark { get; set; }
        public string SearchId { get; set; }
        public string PNRId { get; set; }
        public string TicketId { get; set; }
        public decimal HoldCharge { get; set; }
        public bool IsHoldByAgent { get; set; }
        public DateTime HoldDateTime { get; set; }
        public decimal CreditNode { get; set; }
        public decimal DebitNode { get; set; }
        public string PGStatus { get; set; }
        public string TicketStatus { get; set; }
        public string FamilyFare { get; set; }
        public decimal GSTDIFF { get; set; }
        public DateTime GstUpdateDateTime { get; set; }
        public bool BookedByStaff { get; set; }
        public string StaffId { get; set; }
        public string CouponCode { get; set; }
        public string OldOrderId { get; set; }
        public string OldAirlinePnr { get; set; }
        public DateTime OldPnrDateTime { get; set; }
        public List<FlightTicketReportModel> TicketReportlist { get; set; }

        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string OrderId { get; set; }
        public string PNR { get; set; }
        public string AirlinePNR { get; set; }
        public string PaxName { get; set; }
        public string TicketNo { get; set; }
        public string Trip { get; set; }
    }

    public class FlightLedgerReportModel
    {
        public string AgencyID { get; set; }
        public string AgentID { get; set; }

        public string AgencyName { get; set; }
        public string InvoiceNo { get; set; }
        public string PnrNo { get; set; }
        public string Aircode { get; set; }
        public string TicketNo { get; set; }
        public string TicketingCarrier { get; set; }
        public string YatraAccountID { get; set; }
        public string AccountID { get; set; }
        public string ExecutiveID { get; set; }
        public string Debit { get; set; }
        public string VC { get; set; }
        public string Credit { get; set; }
        public string Aval_Balance { get; set; }
        public string CreatedDate { get; set; }
        public string BookingType { get; set; }
        public string Remark { get; set; }
        public string C { get; set; }
        public string D { get; set; }
        public string PaymentMode { get; set; }
        public string DueAmount { get; set; }
        public string CreditLimit { get; set; }
        public string TransType { get; set; }
        public string DistrAgencyID { get; set; }
        public List<FlightLedgerReportModel> LedgerReportlist { get; set; }
        public string Loginid { get; set; }
        public string UserType { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string SearchType { get; set; }
        public int Totalcount { get; set; }

    }
    public class FlightTicketFilter
    {
        public string Loginid { get; set; }
        public string UserType { get; set; }
        public string FromDate { get; set; }
        public string Suppliorid { get; set; }
        public string ToDate { get; set; }
        public string OrderId { get; set; }
        public string PNR { get; set; }
        public string AirlinePNR { get; set; }
        public string PaxName { get; set; }
        public string TicketNo { get; set; }
        public string AgentId { get; set; }
        public string Trip { get; set; }
        public string Status { get; set; }
    }

    public class FlightLedgerFilter
    {
        public string Loginid { get; set; }
        public string UserType { get; set; }
        public string FromDate { get; set; }
        public string BookingType { get; set; }
        public string ToDate { get; set; }
        public string AgentID { get; set; }
        public string SearchType { get; set; }
        public string TransType { get; set; }
    }

    public enum StatusClass
    {
        Pending,
        InProcess,
        Ticketed,
        Rejected,
        Cancelled,
        CancelRequest,
        CancelInprocess,
        CancelRejecRejectt,
        ReIssueRequest,
        ReIssueInProcess,
        ReIssue,
        ProxyRequest,
        ProxyInProcess,
        Confirm,
        Hold
    }
}