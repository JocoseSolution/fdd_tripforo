﻿using PagedList;
using FixedDD.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace FixedDD.Controllers
{
    public class AdtFareDetailsController : Controller
    {
        private myAmdDB db = new myAmdDB();

        // GET: AdtFareDetails
        public ActionResult Index(int? page, string search)
        {
            IQueryable<AdtFareDetails> stdList;
            int totalskip;
            GetIndex(page, search, out stdList, out totalskip);
            return View(stdList.Skip(totalskip).Take(20));
        }
        private void GetIndex(int? page, string search, out IQueryable<AdtFareDetails> stdList, out int totalskip)
        {
            int pageno = page ?? 1;
            totalskip = (pageno - 1) * 20;
            if (string.IsNullOrEmpty(search))
                stdList = db.AdtFareDetails.OrderByDescending(b => b.id);
            else
                stdList = db.AdtFareDetails;
            ViewBag.totalrecord = stdList.Count();
        }
        // GET: AdtFareDetails/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AdtFareDetails adtFareDetails = db.AdtFareDetails.Find(id);
            if (adtFareDetails == null)
            {
                return HttpNotFound();
            }
            return View(adtFareDetails);
        }

        // GET: AdtFareDetails/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: AdtFareDetails/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,AdtFare,AdtBfare,AdtTax,AdtFSur,AdtYR,AdtWO,AdtIN,AdtQ,AdtJN,AdtOT,AdtSrvTax,AdtSrvTax1,AdtEduCess,AdtHighEduCess,AdtTF,ADTAdminMrk,ADTAgentMrk,ADTDistMrk,AdtDiscount,AdtDiscount1,AdtCB")] AdtFareDetails adtFareDetails)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.AdtFareDetails.Add(adtFareDetails);
                    db.SaveChanges();
                    TempData["Msg"] = 1;
                }
            }
            catch (Exception ex)
            {
                if (ex.ToString().Contains("Duplicate_QuickMenu"))
                    TempData["Msg"] = 0;
            }
            return RedirectToAction("Index");

        }

        // GET: AdtFareDetails/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AdtFareDetails adtFareDetails = db.AdtFareDetails.Find(id);
            if (adtFareDetails == null)
            {
                return HttpNotFound();
            }
            return View(adtFareDetails);
        }

        // POST: AdtFareDetails/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,AdtFare,AdtBfare,AdtTax,AdtFSur,AdtYR,AdtWO,AdtIN,AdtQ,AdtJN,AdtOT,AdtSrvTax,AdtSrvTax1,AdtEduCess,AdtHighEduCess,AdtTF,ADTAdminMrk,ADTAgentMrk,ADTDistMrk,AdtDiscount,AdtDiscount1,AdtCB")] AdtFareDetails adtFareDetails)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(adtFareDetails).State = EntityState.Modified;
                    db.SaveChanges();
                    TempData["Msg"] = 2;
                }
            }
            catch (Exception ex)
            {
                if (ex.ToString().Contains("Duplicate_Id"))
                    TempData["Msg"] = 0;
            }
            return RedirectToAction("Index");
        }

        // GET: AdtFareDetails/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AdtFareDetails adtFareDetails = db.AdtFareDetails.Find(id);
            if (adtFareDetails == null)
            {
                return HttpNotFound();
            }
            return View(adtFareDetails);
        }

        // POST: AdtFareDetails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                AdtFareDetails adtFareDetails = db.AdtFareDetails.Find(id);
                db.AdtFareDetails.Remove(adtFareDetails);
                db.SaveChanges();
                TempData["Msg"] = 3;
            }
            catch (Exception ex)
            {

                TempData["Msg"] = 4;
            }
            return RedirectToAction("Index");
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
