﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FixedDD.Models;

namespace WebApplication2.Controllers
{
    public class AirLinesNamesController : Controller
    {
        private myAmdDB db = new myAmdDB();

        public ActionResult Index(int? page, string search)
        {
            IQueryable<AirLinesName> stdList;
            int totalskip;
            GetIndex(page, search, out stdList, out totalskip);
            return View(stdList.Skip(totalskip).Take(20));
        }
        public PartialViewResult NextPage(int? page, string search)
        {
            IQueryable<AirLinesName> stdList;
            int totalskip;
            GetIndex(page, search, out stdList, out totalskip);
            return PartialView(stdList.Skip(totalskip).Take(20));
        }
        private void GetIndex(int? page, string search, out IQueryable<AirLinesName> stdList, out int totalskip)
        {
            int pageno = page ?? 1;
            totalskip = (pageno - 1) * 20;
            if (string.IsNullOrEmpty(search))
                stdList = db.AirLinesName.OrderBy(b => b.id);
            else
                stdList = db.AirLinesName.Where(b => b.AL_Code.Contains(search)).OrderBy(b => b.id);
            ViewBag.totalrecord = stdList.Count();
        }
        // GET: AirLinesNames/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AirLinesName airLinesName = db.AirLinesName.Find(id);
            if (airLinesName == null)
            {
                return HttpNotFound();
            }
            return View(airLinesName);
        }

        // GET: AirLinesNames/Create
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public JsonResult GetAirLinesName(string Prefix)
        {

            var GetAirLinesName = (from c in db.AirLinesName
                               where c.AL_Name.StartsWith(Prefix) || c.AL_Code.StartsWith(Prefix)
                               select new
                               {
                                   c.AL_Name,
                                   c.AL_Code,                                 

                               }).ToList();



            if (GetAirLinesName != null)
            {
                //  var BindInf = GetCityData.
            }


            return Json(GetAirLinesName, JsonRequestBehavior.AllowGet);
        }
        // POST: AirLinesNames/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,AL_Code,AL_Name,CreatedDate")] AirLinesName airLinesName)
        {
            if (ModelState.IsValid)
            {
                db.AirLinesName.Add(airLinesName);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(airLinesName);
        }

        // GET: AirLinesNames/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AirLinesName airLinesName = db.AirLinesName.Find(id);
            if (airLinesName == null)
            {
                return HttpNotFound();
            }
            return View(airLinesName);
        }

        // POST: AirLinesNames/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,AL_Code,AL_Name,CreatedDate")] AirLinesName airLinesName)
        {
            if (ModelState.IsValid)
            {
                db.Entry(airLinesName).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(airLinesName);
        }

        // GET: AirLinesNames/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AirLinesName airLinesName = db.AirLinesName.Find(id);
            if (airLinesName == null)
            {
                return HttpNotFound();
            }
            return View(airLinesName);
        }

        // POST: AirLinesNames/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            AirLinesName airLinesName = db.AirLinesName.Find(id);
            db.AirLinesName.Remove(airLinesName);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
